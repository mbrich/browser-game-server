/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Network;

import com.level65.GameServer.Network.Protocol.IProtocol;
import java.io.DataOutputStream;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class ClientOutputHandler implements Runnable
{
    
    private int clientId;
    private IProtocol protocol;
    DataOutputStream toClient;
    static final Logger LOG = 
            LoggerFactory.getLogger(ClientOutputHandler.class);
    
    public ClientOutputHandler( int _clientId, DataOutputStream _toClient, 
            IProtocol _protocol ) throws IOException
    {
        this.toClient = _toClient;
        this.clientId = _clientId;
        this.protocol = _protocol;
        
        
    }
    
    
    private void closeOutputStream()
    {
        try
        {
            this.toClient.close();
        
        }
        catch ( IOException e )
        {
            
            
        }
    }
    
    
    
    public void run()
    {

        for ( ;; )
        {
            try
            {
               synchronized ( ClientMgr.getQueue( clientId ) )
               {
                   while ( ClientMgr.getQueue( clientId ).isEmpty() )
                   {
                       ClientMgr.getQueue( clientId ).wait();
                       
                   }
                   
                   ClientState state = ClientMgr.getState( clientId );
                   
                   if ( state == ClientState.OPEN || state == ClientState.AUTHENTICATING )
                   {
                    String msg = ClientMgr.bufferPop( this.clientId );
                    if ( msg.equals( "PING" ) )
                    {
                         this.protocol.sendPing( this.toClient );

                    }
                    else if ( msg.equals( "CLOSE" ) )
                    {
                        this.protocol.sendClose( this.toClient );

                    }
                    else
                    {
                         this.protocol.sendMessage( this.toClient, msg );

                    }
                   }
                   else
                   {
                       LOG.error( "Unable to send message.  Client is " +
                               " disconnecting. State is {}", 
                               ClientMgr.getState( clientId ) );
                       
                   }
               }
                
            }
            catch ( InterruptedException e )
            {
                // Terminate the thread by returning.
                ///System.out.println( "Output Handler interrupted" );
                this.protocol.sendClose( toClient );
                this.closeOutputStream();

                return;
                
            }
        }
    }
    
    
}

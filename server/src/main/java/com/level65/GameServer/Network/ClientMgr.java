/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * DESCRIPTION:
 * This client list controls access to each of the individual clients.
 * 
 * USAGE:
 *  Register New Client
 *      ClientMgr.register( inputThread, client );
 * 
 *  Unregister client:
 *      ClientMgr.unregister( clientId );
 *      
 */
package com.level65.GameServer.Network;

import com.level65.GameServer.Exceptions.InvalidClientIdException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Kayvan Boudai and Michael Brich
 */
public class ClientMgr 
{
    static final org.slf4j.Logger LOG = 
            LoggerFactory.getLogger(ClientMgr.class);    
    
    private static int nextClientId = -1;
    private static ArrayList<ClientInfo> clientList = 
            new ArrayList<ClientInfo>();

    private static int clientCount = 0;
   
    // Private constructor to prevent class from being instantiated.
    private ClientMgr() {}
    
    
    /**
     * Get the number of clients currently connected to the server.
     * @return 
     */
    public static int size()
    {
        return clientCount;
        
    }
    
    
    /**
     * Registers a client's connection with the server, making it available to
     * other clients on the server.
     * @param client 
     */
    public synchronized static void register( Thread inputThread, Socket client )
    {
        clientList.add( ++nextClientId, new ClientInfo( inputThread, client ) );
        ++clientCount;
        
    }
    
    
    /**
     * Unregisters a client's connection to the server.  This method MUST be
     * synchronized to prevent multiple threads from simultaneously removing
     * clients (data corruption will occur).
     * @param clientId 
     */
    public synchronized static void unregister( int clientId )
    {
        System.out.println( "Unregister" );
        // Here we are using ArrayList.set instead of Arraylist.remove for the
        // very specific reason that remove shifts elements to the left, which
        // is catastrophic to our client list. We also avoid reusing clientIds
        // to prevent a new client from receiving potentially expired requests
        // from other clients.  
        if ( hasClient( clientId ) ) clientList.set( clientId, null );
        --clientCount;
        
    }
    
    
    /**
     * Handles the disconnect procedure when the server receives a client's
     * disconnect/close packet.  
     * @param clientId 
     */
    public static void requestDisconnect( int clientId )
    {
        ClientMgr.getInfo(clientId).bufferOut.add( "CLOSE" );
        
        
    }
    
    
    public static ClientState getState( int clientId )
    {
        synchronized ( ClientMgr.getInfo( clientId ).clientState )
        {
            return ClientMgr.getInfo( clientId ).clientState;
            
        }
        
    }
    
    /**
     * Sets the client's connection state.  This will change over the course of
     * the client's connection.  It is important that this be set correctly, as
     * the client will not receive all data during certain phases of the
     * connection.
     * @param clientId
     * @param _state 
     */
    public static void setState( int clientId, ClientState _state )
    {
        synchronized ( ClientMgr.getInfo(clientId).clientState )
        {
            ClientMgr.getInfo( clientId ).clientState = _state;
        
        }
    }
    
    /**
     * Sets the reference to the client's Output Thread.  We must have a
     * reference to the thread so that it can be shutdown when the clientThis 
     * cannot be set when the connection is registered, because the outgoing 
     * thread is not started until the client handshake is complete.
     * @param clientId
     * @param outputThread 
     */
    public static void setOutputThread( int clientId, Thread _outputThread )
    {
        ClientInfo tmp = clientList.get( clientId );
        synchronized ( tmp.dataLock )
        {
            tmp.outputThread = _outputThread;
            
        }
        //clientList.get( clientId ).setOutputThread( outputThread );
        
    }
    
    /**
     * Returns the time that the client connected to the server. Useful for
     * figuring out how long the client has been connected.
     * @param clientId
     * @return 
     */
    public static long getConnectTime( int clientId )
    {
        return clientList.get( clientId ).connectTime;
        
    }
    
    
    /**
     * Returns a thread reference to the thread running this particular client.
     * Useful for getting a reference to use with thread methods like wait
     * and notify.
     * @param clientId
     * @return 
     */
    public static Thread getInputThread( int clientId )
    {
        synchronized ( clientList.get( clientId ).dataLock )
        {
            return clientList.get( clientId ).inputThread;
        
        }
    }
    
    
    /**
     * Returns a handle to this client's output thread, if one exists.  This is
     * necessary for clean shutdowns/disconnects so that the ClientList can
     * close a client's threads when it disconnects.
     * @param clientId
     * @return 
     */
    public static Thread getOutputThread( int clientId )
    {
        synchronized ( clientList.get( clientId ).dataLock )
        {
            return clientList.get( clientId ).outputThread;
        
        }
    }
    
    
    
    /**
     * Returns the IP address of the client as a string.  
     * @param clientId
     * @return 
     */
    public static String getIp( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return clientList.get( clientId ).client.getInetAddress().toString();
        
        }
        else
        {
            return "";
            
        }
    }
    
    
    
    /**
     * Pushes a msg onto the client's outgoing buffer.  This method MUST NOT be
     * made synchronized.  Each buffer is thread-safe, so that threads only
     * have to wait if they are trying to write to the same buffer.  Multiple
     * threads can safely write to DIFFERENT buffers simultaneously, as long as
     * this method remains NOT synchronized.
     * @param clientId
     * @param msg 
     */
    public static void bufferPush( int clientId, String msg )
    {
        ClientInfo tmp = clientList.get( clientId );
        
        synchronized ( tmp.bufferOut )
        {
            tmp.bufferOut.add( msg );
            tmp.bufferOut.notify();
            
        }
        
        /*
        if ( hasClient( clientId ) ) 
        {
            clientList.get( clientId ).bufferPush( msg );
   
        }*/
    }
    
    
    /**
     * Returns the first item in the buffer, or null if the buffer is currently
     * empty. Each buffer is thread-safe, so that threads only
     * have to wait if they are trying to write to the same buffer.  Multiple
     * threads can safely write to DIFFERENT buffers simultaneously, as long as
     * this method remains NOT synchronized.
     * @param clientId
     * @return 
     */
    public static String bufferPop( int clientId )
    {
        String item = null;
        if ( !hasClient( clientId ) ) return "";
        
        try 
        {
            return clientList.get( clientId ).bufferPop();
        
        }
        catch ( InterruptedException e )
        {
            LOG.error( "Exception: {}", e );
        }
        
        return item;
    }
    
    
    
    public static void updateHeartbeat( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            ClientInfo clientInfo = clientList.get( clientId );
            synchronized ( clientInfo.dataLock )
            {
                clientInfo.lastHeartbeatTime = System.currentTimeMillis();
                
            }

        }   
    }
    
    
    /**
     * Returns the connection port on the client side.
     * @param clientId
     * @return 
     */
    public static int getPort( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return clientList.get( clientId ).client.getPort();
        
        }
        else
        {
            return 0;
            
        }
    }
    
    
    /**
     * Returns the client's current username.
     * @param clientId
     * @return 
     */
    public static String getUsername( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return clientList.get( clientId ).username;
        
        }
        else
        {
            return "";
            
        }
    }
    
    
    /**
     * Returns the timestamp for the last time the server was able to read
     * data from the client socket successfully.  This is used to determine
     * disconnect times.
     * @param clientId
     * @return 
     */
    public static long getLastHeartbeatTime( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            ClientInfo clientInfo = clientList.get( clientId );
            synchronized ( clientInfo.dataLock )
            {
                return clientInfo.lastHeartbeatTime;
                
            }
        }
        else
        {
            return -1;
        
        }
    
    }
    
    /**
     * Set's the username for a specific client.
     * @param clientId
     * @param _username 
     */
    public synchronized static void setUsername( int clientId, String _username )
    { 
       if ( hasClient( clientId ) )
        {
            clientList.get( clientId ).username = _username;
        
        }   
    }
    
    
    
    public static ClientInfo getInfo( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return clientList.get( clientId );
            
        }
        else
        {
            return null;
            
        }
        
    }
    /**
     * Returns the master client list as an ArrayList.  
     * @return 
     */
    public static ArrayList<ClientInfo> getList()
    {
        return clientList;
        
    }
    
    
    /**
     * Returns the current size of the master client list. Useful for knowing
     * how many clients are currently registered with the server.
     * @return 
     */
    public static int getListSize()
    {
        return clientList.size();
        
    }
    
    
    /**
     * Gets the current queue from the client info object.
     * @param clientId
     * @return 
     */
    public static Queue<String> getQueue( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return clientList.get( clientId ).bufferOut;
        
        }
        else
        {
            // TODO: Check if null is acceptable as a return.
            return null;
            
        }
    }
    
    
    /**
     * Sets the client's current sessionId.  A client may only be in one session
     * at a time.
     * @param clientId
     * @param chunkId 
     */
    public static void setSessionId( int clientId, int chunkId )
    {
        if ( hasClient( clientId ) )
        {
            clientList.get( clientId ).setSessionId( chunkId );
            
        }
    }
    
    
    /**
     * Get the client's current sessionId. A client may only be in one session
     * at a time.
     * @param clientId
     * @return 
     */
    public static int getSessionId( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return clientList.get( clientId ).sessionId;
            
        }
        else
        {
            return -1;
            
        }
        
    }
    
    
    
    public static void setLevelId( int clientId, int levelId )
    {
        if ( hasClient( clientId ) )
        {
            clientList.get( clientId ).setLevelId( levelId );
            
        }
    }
    
    public static int getLevelId( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return clientList.get( clientId ).playerLevelId;
            
        }
        else
        {
            return -1;
            
        }
        
    }
    
    
    public static void setEntityId( int clientId, int entityId )
    {
        if ( hasClient( clientId ) )
        {
            clientList.get( clientId ).setEntityId( entityId );
              
        }
    }
    
    
    
    public static int getEntityId( int clientId )
    {
        if ( hasClient( clientId ) )
        {
            return clientList.get( clientId ).entityId;
            
        }
        else
        {
            return -1;
            
        }
    }
    
    
    
    /**
     * Returns the clientId that will be assigned to the next client that
     * connects to the server and registers a connection.
     * @return 
     */
    public static int getNextClientId()
    {
       return nextClientId + 1;
       
    }
    
    
    /**
     * Returns whether the ClientList has a client with the specified clientId.
     * Throws an InvalidClientIdException when a clientId does not exist.
     * @param clientId
     * @return 
     */
    public static boolean hasClient( int clientId )
    {
        
        try
        {
            if ( clientList.get( clientId ) != null )
            {
                return true;

            }
            else
            {
                throw new InvalidClientIdException( "Client ID not found." );
                
            }
        
        }
        catch ( InvalidClientIdException e )
        {
            e.printStackTrace();
            
        }
        
        return false;
         
    }
}

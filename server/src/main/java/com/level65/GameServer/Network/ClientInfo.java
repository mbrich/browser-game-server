/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
 * DESCRIPTION:
 * The ClientInfo object is managed by the Clients class.  
 */
package com.level65.GameServer.Network;

import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;


/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class ClientInfo
{
   
    public Socket client;
    public long connectTime;
    public long lastHeartbeatTime;
    ClientState clientState = ClientState.DISCONNECTED;
    // Do not access bufferOut directly.  It must be accessed through the
    // synchronized push and pop in order to stay thread-safe.
    // TODO: May be upgraded to a PriorityQueue in the future.
    public final Queue<String> bufferOut;
    public String username = "";
    public Thread inputThread;
    public Thread outputThread;
    
    public int entityId = -1;
    public int playerLevelId = -1;
    public int sessionId = -1;
    
    
    // Locks
    public final Object dataLock = new Object();
    
    public ClientInfo( Thread _inputThread, Socket _client )
    {
        this.bufferOut = new ConcurrentLinkedQueue<String>();
        this.client = _client;
        this.inputThread = _inputThread;
        this.connectTime = System.currentTimeMillis();
        this.lastHeartbeatTime = System.currentTimeMillis();
        
    }
    

    
    public void setState( ClientState state )
    {
        synchronized( this.clientState )
        {
            this.clientState = state;
            
        }
    }
    

    
    public void bufferPush( String msg )
    {
        synchronized ( this.bufferOut )
        {   
            this.bufferOut.add( msg );
            this.bufferOut.notify();
            
        }
    }
    
    
    /**
     * Pop a msg from the client's outgoing buffer so that it can be sent out
     * on the outgoing socket connection.  This method MUST be synchronized
     * to prevent multiple threads from popping data at once.
     */
    public String bufferPop() throws InterruptedException
    {
        return this.bufferOut.poll();
    
    }
    
    
    public void setEntityId( int entityId )
    {
        synchronized ( this.dataLock )
        {
            this.entityId = entityId;
            
            
        }    
    }
    
    
    public void setLevelId( int levelId )
    {
        synchronized ( this.dataLock )
        {
            this.playerLevelId = levelId;
            
        }    
    }
    
    
    public void setSessionId( int sessionId )
    {
        synchronized ( this.dataLock )
        {
            this.sessionId = sessionId;
            
        }
    }
    
    
    
    public long getLastHeartbeatTime()
    {
        synchronized ( this.dataLock )
        {
            return this.lastHeartbeatTime;
        
        }
    }
    
   
}


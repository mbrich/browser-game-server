/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Network.Protocol;

import java.io.UnsupportedEncodingException;

/**
 * @author Kayvan Boudai and Michael Brich
 */
public class WebsocketDataFrame13 implements IDataFrame
{

    private byte opcode = 0x0;
    private boolean isControlFrame = false;
    private boolean isCloseFrame = false;
    private boolean isPing = false;
    private boolean isPong = false;
    private String frameType = "";
    private String text = "";
    
    public WebsocketDataFrame13( byte[] headerBytes, byte[] payload )
    { 
        System.out.println( "New websocket dataframe" );
        try 
        {
            this.opcode = (byte)( headerBytes[0] & (byte) 127 );
            this.text = new String( payload, "UTF-8" );
        
        }
        catch ( UnsupportedEncodingException e )
        {
            
        }
    }
    
    public boolean isText()
    {
        return ( this.opcode == 0x1 );
        
    }
    
    
    @Override
    public boolean isControlFrame() 
    {
        return false;
        
    }

    public String frameType()
    {
        return this.frameType;
        
    }
    
    public boolean isCloseFrame()
    {
        return ( this.opcode == 0x8 );
        
    }
    
    @Override
    public boolean isPing() 
    {
        System.out.println( "[PING] Opcode: " + this.opcode );
        return ( this.opcode == 0x9 );
    }

    @Override
    public boolean isPong() 
    {
        System.out.println( "[PONG] pcode: " + this.opcode );
        return ( this.opcode == 0xA );
        
    }

    @Override
    public String text() 
    {
        return this.text;
    }

    
    
}

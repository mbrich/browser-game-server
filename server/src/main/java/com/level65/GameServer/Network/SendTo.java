/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Network;

import com.level65.GameServer.Game.GameMgr;
import com.level65.GameServer.Game.SessionMgr;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class SendTo
{
    /**
     * Send a message to a specific client.
     * @param clientId
     * @param msg 
     */
    public static void send( int clientId, ClientMsg clientMsg )
    {
        String newMsg = clientMsg.msg + "\037";
        ClientMgr.bufferPush( clientId, newMsg );
        
    } // ef
    
   
   
    /**
     * Sends a message to all currently connected clients.
     * @param sourceClientId
     * @param clientMsg 
     */
    public static void all( ClientMsg clientMsg )
    {
        int i = 0;
        for ( ClientInfo info : ClientMgr.getList() )
        {
            if ( info != null ) send( i, clientMsg );
            
        }
    }
   
    
    /**
     * Convenience method that sends a message to all clients in a session
     * EXCLUDING the source client.  Performs a sessionId lookup for you and 
     * only requires the clientId and clientMsg parameters.
     * session.
     * @param clientMsg 
     */
    public static void mySession( int sourceClientId, ClientMsg clientMsg )
    {
        int sessionId = ClientMgr.getSessionId( sourceClientId );       
        session( sessionId, sourceClientId, clientMsg, true );
        
    }
    
    /**
     * Convenience method that sends a message to all clients in a session,
     * EXCLUDING the source client. 
     * @param sessionId
     * @param sourceClientId
     * @param clientMsg 
     */
    public static void session( int sessionId, int sourceClientId, 
            ClientMsg clientMsg )
    {
        session( sessionId, sourceClientId, clientMsg, true );
    }
    
    
    /**
     * Convenience method that sends a message to ALL clients in a session, 
     * INCLUDING the source client. All parameters except sessionId and 
     * clientMsg can be omitted when calling this.
     * @param sessionId
     * @param clientMsg 
     */
    public static void session( int sessionId, ClientMsg clientMsg )
    {
        session( sessionId, -1, clientMsg, false );
        
    }
    
    /**
     * Sends a message to all clients in the specified session.  This method
     * can exclude or include the source client depending on the flags set.
     * See mySession and other overloaded session methods for the available
     * convenience methods.
     * @param sessionId
     * @param clientMsg 
     */
    public static void session( int sessionId, 
            int sourceClientId, 
            ClientMsg clientMsg, 
            boolean excludeSender )
    {

        for ( int clientId : SessionMgr.clients( sessionId ) )
        {
            if ( excludeSender && clientId != sourceClientId 
                    && sourceClientId > 0 ) send( clientId, clientMsg );
            
        }
    }
    
    
    
   
}

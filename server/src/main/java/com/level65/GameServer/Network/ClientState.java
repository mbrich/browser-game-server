/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.level65.GameServer.Network;

/**
 * Client state is external and different from the socket state.  It tells us
 * what connection phase the Client is in.  We use this because there are times,
 * such as the opening and closing handshakes, where our socket is able to send
 * and receive data, but the connection is not yet ready for real use.  
 * 
 * OPENING_HANDSHAKE
 * The client is handshaking with the server.  At this point the client is
 * technically connected, but we do not want to treat it as connected until it
 * has successfully finished handshaking.
 * 
 * AUTHENTICATING (OPTIONAL)
 * This step is used when authentication is involved.  This step is like a
 * second handshake, because the client may be connected to the server, but
 * should not be treated as connected until they have logged in. This step is
 * optional and may be skipped over if not used.
 * 
 * OPEN
 * The client and server may freely exchange data.  This is the main state for
 * a healthy connection and should remain this way as long as the client is
 * active.  The server will ping OPEN clients every few seconds to maintain a
 * heartbeat.
 * 
 * CLOSING_HANDSHAKE
 * Occurs when the client sends a Close packet.  In this state the client is
 * asking to disconnect and expecting another close packet in return.  We should
 * push out what is currently queued, but should not queue any additional data.
 * 
 * @author Michael Brich
 */
public enum ClientState 
{ 
    CONNECTING, 
    OPENING_HANDSHAKE, 
    AUTHENTICATING,
    OPEN,
    CLOSING_HANDSHAKE,
    DISCONNECTED 
}
/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Network.Protocol;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

/**
 * @author Kayvan Boudai and Michael Brich
 */
public class TcpProtocol implements IProtocol
{
    public IDataFrame receiveData( DataInputStream fromClient ) throws IOException
    {
        byte[] b = new byte[2];
        fromClient.readFully(b);
        return new TcpDataFrame( b );
        
    }
    
    @Override
    public boolean doHandshake( Scanner scanner, DataOutputStream toClient )
    {
        return true;
        
        
    }
    
    @Override
    public void sendPing( DataOutputStream toClient )
    {
        // TODO: Implement ping in message header.
    }
    
    @Override
    public void sendPong( DataOutputStream toClient )
    {
        //TODO: Implement pong request in message header.
        
    } 
    
    @Override
    public void sendClose( DataOutputStream toClient )
    {
        try
        {
            toClient.writeChars( "close" );
            
        }
        catch( IOException e )
        {
            
        }
        
    }
    
    public void sendMessage( DataOutputStream toClient, String msg ) 
    {
        try
        {
            toClient.writeBytes( msg );
            toClient.flush();
        
        }
        catch ( IOException e )
        {
            e.printStackTrace();
            
        }
  
    }
}
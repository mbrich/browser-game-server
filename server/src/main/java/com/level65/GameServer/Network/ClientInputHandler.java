/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Network;

import com.level65.GameServer.Network.Protocol.IDataFrame;
import com.level65.GameServer.Network.Protocol.IProtocol;
import com.level65.GameServer.Commands.CommandHandler;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ClientInputHandler implements Runnable 
{
    static final Logger LOG = 
            LoggerFactory.getLogger(ClientInputHandler.class);
    
    Socket client;
    DataOutputStream toClient;
    DataInputStream fromClient;
    IProtocol protocol;
    StringTokenizer tokenizer;
    int clientId;
    
    public ClientInputHandler( int _clientId, Socket Client, 
            IProtocol _protocol ) 
    {
        this.client = Client;
        this.clientId = _clientId;
        //this.clientState = ClientState.CONNECTING;
        
        // Protocol set to 13 for now, should be adjustable in the future
        // if we need to implement new handling but also maintain legacy compat.
        this.protocol = _protocol;
        
        
        try 
        {
            this.toClient = new DataOutputStream(this.client.getOutputStream());
            this.fromClient = new DataInputStream(this.client.getInputStream());
            
        } 
        catch ( IOException e ) 
        {
            // TODO: Handle exception
            System.err.printf( "IOE: %s%n", e.getMessage() );
            e.printStackTrace();
            
        }
    } // ef
    
    
    
    @Override
    public void run() 
    {
        try 
        {
            LOG.info( "New client connected from IP {}", 
                    client.getInetAddress().toString() );

            Scanner scanner = new Scanner( client.getInputStream() );
            
            this.protocol.doHandshake( scanner, this.toClient );
            
            LOG.info( "Passed handshake" );
            
            ClientMgr.setState( clientId, ClientState.OPEN);
            
            ClientOutputHandler clientOutputHandler =
                new ClientOutputHandler( this.clientId, this.toClient, 
                    this.protocol );
           
           Thread outputThread = new Thread( clientOutputHandler );
           outputThread.start();
           
           
           ClientMgr.setOutputThread( this.clientId, outputThread );
           while ( !Thread.currentThread().isInterrupted() ) 
           {
                try
                {
                    IDataFrame dataFrame = this.protocol.receiveData( 
                            this.fromClient );

                    LOG.info( "Data frame received" );
                    if ( dataFrame != null )
                    {
                        if ( dataFrame.isText() )
                        {
                            LOG.info( "received message" );
                            this.processMessage( dataFrame.text() );

                        }
                        else if ( dataFrame.isPing() )
                        {
                            LOG.trace( "Received ping" );
                            ClientMgr.updateHeartbeat(clientId);
                            protocol.sendPong( this.toClient );
                            
                        }
                        else if ( dataFrame.isPong() )
                        {
                            LOG.info( "Pong received" );
                            // Sets the client's last heartbeat to right now.
                            // The server disconnects clients that have not
                            // sent a heartbeat every so many seconds.
                            ClientMgr.updateHeartbeat(clientId);
                            
                            
                        }
                        else if ( dataFrame.isCloseFrame() )
                        {
                            System.out.printf( "Close Frame received" );
                            //this.close();
                            // We request that the disconnect process be
                            // started for this user.
                            ClientMgr.requestDisconnect(this.clientId);

                        }
                    }   
                }
                catch ( IOException e )
                {
                    if ( Thread.currentThread().isInterrupted() )
                    {
                        // Specific case
                        //System.out.println( "INTERRUPTED" );
                        
                    }
                    else
                    {
                        e.printStackTrace();
                    
                    }
                    
                }
           }
           
           System.out.println( "Terminating input handler.");
           //this.close();
           return;
           
            // TODO: add disconnect sequence here (maybe send close message to 
            // client then delete from client list
            
        } 
        catch ( IOException e )//| InterruptedException e ) 
        {
            //: Handle exn or throw it further up the stack.
            e.printStackTrace();
    
        }
    } // ef
    
    
    /**
     * Sends a message to the client. The message is passed through the
     * protocol specific sendMessage function and then sent out through the
     * socket.
     * @param msg 
     */
    public void sendMessage( String msg )
    {
        this.protocol.sendMessage( this.toClient, msg );
        
    }
  

    
    public void processMessage( String msg )
    {
            
        ArrayList<String> tokens = new ArrayList<String>();
        
        if ( msg != null )
        {
            this.tokenizer = new StringTokenizer( msg, "\037" );
            
            while ( this.tokenizer.hasMoreTokens() )
            {
		tokens.add( this.tokenizer.nextToken() );
                
            }
            
            if ( tokens.size() > 0 )
            {
               
                try 
                {
                    short commandName = Short.parseShort(tokens.get( 0 ));

                    // Remove the command so that the first arg is index 0.
                    tokens.remove( 0 );
                    if ( CommandHandler.doCommand( this.clientId, commandName, 
                            tokens ) )
                    {
                        System.out.printf( "[BGServer] Command done: %s%n", 
                                commandName );
                    }
                    else
                    {
                        // Do something for invalid command.
                        System.out.printf( "[BGServer] Command failed: %s%n", 
                                commandName );
                    }
                }
                catch ( NumberFormatException e )
                {
                    System.err.printf( "[BGServer] Invalid command: %s%n",
                            tokens.get( 0 ) );
                    
                }
            }
            else
            {
            	System.err.println("[BGServer] Recieved null from client");
            	//TODO: handle this null error better
            	//this.close();
            }
        }
    }
 
}
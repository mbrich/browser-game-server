/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * Description:
 * This class becomes its own thread at runtime.  It sleeps most of the time,
 * but iterates over the client list every few seconds and sends out pings
 * to clients.  Clients that have not responded to a server ping for several
 * intervals/cycles (the time between 2 pings from the server) are disconnected
 * from the server.
 * 
 * Usage:
 * None.  This class runs itself and does not provide methods that can be called
 * externally.
 */
package com.level65.GameServer.Network;

import java.io.IOException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 
 * @author Kayvan Boudai and Michael Brich
 */
public class ConnectionManager implements Runnable 
{
    static final Logger LOG = 
            LoggerFactory.getLogger(ConnectionManager.class);
    
    
    public ConnectionManager()
    {
        
    }

    
    @Override
    public void run() 
    {
        
        
        for ( ;; )
        {
            synchronized( ClientMgr.getList() )
            {
                
                // We need to check all our current clients for timeouts.
                //for ( ClientInfo clientInfo : ClientList.getList() )
                
                ArrayList<Integer> deadClients = new ArrayList<Integer>();
                
                int clientId = 0;
                for ( ClientInfo client : ClientMgr.getList() )
                {
                    
                    if ( client != null )
                    {
                        if ( ( System.currentTimeMillis() - 
                            client.getLastHeartbeatTime() > 6000 ) ||
                                client.clientState == ClientState.DISCONNECTED )
                        {
                        
                            // Add a user to our temp list.
                            deadClients.add(clientId);

                        }
                        else
                        {
                            client.bufferPush("PING");

                        }
                    }
                    ++clientId;
                    
                }

                
                int droppedClients = 0;
                for ( Integer deadClientId : deadClients )
                {
                    ClientInfo client = ClientMgr.getInfo(deadClientId);

                    if ( client.outputThread != null ) 
                        client.outputThread.interrupt();

                    if ( client.inputThread != null )
                        client.inputThread.interrupt();
                
                    LOG.info( "{} is being dropped for lost connection.",
                            client.username );
                    
                    ClientMgr.unregister(deadClientId);
                    droppedClients++;
                    
                }
                
                deadClients = null;
                if ( droppedClients > 0 )
                {
                    LOG.info( "{} dead clients were dropped.", 
                            droppedClients );
                    
                }
                    
                // We sleep to conserve resources.  This thread only needs to be
                // active once every few seconds to check all the connections.

            }
            
            try 
            {
                Thread.sleep( 1000 );

            } 
            catch ( InterruptedException e ) 
            {
                e.printStackTrace();
                LOG.error( e.getMessage() );

            }
            
        }
        
    }
    
}

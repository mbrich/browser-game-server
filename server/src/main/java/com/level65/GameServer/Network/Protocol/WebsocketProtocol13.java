/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Network.Protocol;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Scanner;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class WebsocketProtocol13 implements IProtocol 
{
    static final Logger LOG = 
            LoggerFactory.getLogger(WebsocketProtocol13.class);	
    
    final String protocolStr = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
    
    /**
     * Constructs the key for Sec-WebSocket-Accept
     * Concats the protocol string onto the client's provided key and gets the 
     * sha1 hash for this string, and finally base64 encode that hash to get our
     * "encoded key".  The encoded key is returned to the client during the
     * handshake.  If this key is incorrect, the client aborts their connection
     * request.
     * @param key
     * @return 
     */
    public String getEncodedKey( String key ) 
    {
        
        try 
        {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            
            byte[] sha1 = md.digest( ( key + this.protocolStr ).getBytes() );
            String encodedKey = new String( Base64.encodeBase64( sha1 ) );

            return encodedKey;
            
        } 
        catch ( NoSuchAlgorithmException e ) 
        {
            
            System.err.println( "Exception: " + e.getMessage() );
            return "INVALID";
            
        }
    } // ef 
    
    @Override
    public void sendPing( DataOutputStream toClient )
    {
        // We use WebSocket's build in handling to ping the client.  We pass it
        // a 4 byte message with no payload.  0x89 indicates our ping.
        // As long as the browser supports WebSocket ping/pong, then it will
        // automatically respond to this with a pong.  
        try
        { 
     
            toClient.write( (byte) 0x89 );
            toClient.write( (byte) 0x02 );
            toClient.write( (byte) 0x00 );
            toClient.write( (byte) 0x00 );
            toClient.flush();
           
        } 
        catch ( IOException e )
        {
            LOG.error( "Exception: {}", e );
         
        }
    }
    
    
    @Override
    public void sendPong( DataOutputStream toClient )
    {
        try
        {
            toClient.write( (byte) 0x8A );
            toClient.write( (byte) 0x02 );
            toClient.write( (byte) 0x00 );
            toClient.write( (byte) 0x00 );
            toClient.flush();
            
        }
        catch ( IOException e )
        {
            LOG.error( "Exception: {}", e );
            
        }
        
    }
    
    
    
    @Override
    /**
     * Sends a message to the client indicating the connection is being closed.
     */
    public void sendClose( DataOutputStream toClient )
    {
        LOG.info( "Replying to Closing handshake" );
        try
        {
            toClient.write( (byte) 0xFF );
            toClient.write( (byte) 0x00 );
           // toClient.write( (byte) 0x00 );
           // toClient.write( (byte) 0x00 );
            toClient.flush();
            
        }
        catch ( IOException e )
        {
            LOG.error( "Exception in closing client connection: {}", e );
            
        }
    }
    
    /**
     * Sends a message to target client using this version of the  WebSocket 
     * protocol.  The DataOutputStream must be provided by the caller, as this 
     * class only formats the outgoing message and hands it off to the provided 
     * stream.
     * @param toClient
     * @param msg 
     */
    public void sendMessage( DataOutputStream toClient, String msg ) 
    {
    	// TODO: Proper op-code handling. Different functions for different op-codes?
    	int msgLength = ( msg.getBytes() ).length;
    	
        try
        {
            // TODO: Add msg type detection and opcode handling.
            toClient.write( (byte) 0x81 );

            if ( msgLength < 126 )
            {
                toClient.write( msgLength );

            }
            else if ( msgLength > 125 && msgLength < 65536  )
            {
                // BE CAREFUL!
                // WS13 specifies that our length must be EXACTLY 2 bytes.
                // We must cast it manually to guarantee the length is 
                // converted into exactly 2 bytes. Although DataOutputStream 
                // can automatically cast our length to bytes and write it to 
                // the stream with one method call, the resulting number of 
                // bytes written to the stream will be the number of bytes in a 
                // word for this system.  For a normal 32-bit word this means 4 
                // bytes are written, and for a 8 bytes for a 64-bit word.
                byte[] byteLength = new byte[2];
                byteLength[0] = (byte) (msgLength >> 8);
                byteLength[1] = (byte) msgLength;
                toClient.write( 126 );
                toClient.write( byteLength );

            
            }
            else if ( msgLength >= 65536 && msgLength < Integer.MAX_VALUE )
            {
                
                toClient.write( 127 );
                
                // Our protocol requires that the length of messages longer than 
                // 65,536 must be given in 8 bytes exactly.  By casting to a
                // long, we guarantee that the DataOutputStream will write
                // exactly 8 bytes to the stream.
                long msgLengthLong = (long) msgLength;
                toClient.writeLong( msgLengthLong );
                
                
            }
            
            // Write our payload into the stream.
            toClient.writeBytes( msg );
        
        }
        catch ( IOException e )
        {
            // TODO: Add exception
            e.printStackTrace();
            
        }
    }

    
    /**
     * 
     * @param fromClient
     * @return
     * @throws IOException 
     */
    @Override
    public IDataFrame receiveData( DataInputStream fromClient ) throws IOException
    {
        
        byte[] b = new byte[2];
        int msgLength = 0;
        if ( fromClient.read( b ) != -1 )
        {
   
            //read in dataLength code
            byte dataLength = b[1];
            byte op = (byte) 127;
            
            //remove mask bit from datalength
            dataLength = (byte)(dataLength & op );
            
            //get message lenth through byte to in conversion
            msgLength = (int) readLength( dataLength, fromClient );
            
            byte[] masks = new byte[4];
        
            //get mask code
            fromClient.read(masks);

            byte[] payload = new byte[msgLength];
            
            //get payload data
            fromClient.read( payload );
        
            byte[] msg = new byte[msgLength];
            
            //convert data into UTF-8 data
            for ( int i = 0; i < msgLength; ++i )
            {
            	msg[i] = (byte) ( payload[i] ^ masks[i % 4] );
            
            
            }
          
            return new WebsocketDataFrame13( b, msg );
            
        
        }
        else
        {
            //TODO: proper fail procedure
            return null;
            
        }  
    }
    
    
    static long readLength( int firstByte, InputStream istr ) throws IOException 
    {
        long length = firstByte & 0x7F;
        switch ( firstByte )
        {
            // 16 bit length
            case 126:
            length = (readByte(istr) << 8) | readByte(istr);
                break;
            
            // 63-bit length
            case 127:
                length = (readByte(istr) << 54) | (readByte(istr) << 48)
                    | (readByte(istr) << 40) | (readByte(istr) << 32)
                    | (readByte(istr) << 24) | (readByte(istr) << 16)
                    | (readByte(istr) << 8) | readByte(istr);
                break;
        }

            return length;
        
    }

    
    
    static int readByte(InputStream istr) throws IOException 
    {
        int val = istr.read();
        if ( val < 0 ) throw new IOException("EOF on read");
       
        return val;
        
    }
    
    
    public boolean doHandshake( Scanner scanner, DataOutputStream toClient ) 
    {
        
        boolean IsKeySet = false;
        boolean IsVerSet = false;
        String EncodedKey = "";
        
        String line = "";
        
        int counter = 0;
        while ( scanner.hasNextLine() && ( line = scanner.nextLine() ) != null )
        {
            String[] tokens = line.split( ": " );
            
            if ( tokens[0].equals( "Sec-WebSocket-Key" ) )
            {
                IsKeySet = true;
                if ( tokens.length >= 2 )
                {
                    System.out.printf( "Client Key: %s%n", tokens[1] );
                    EncodedKey = this.getEncodedKey(tokens[1] );
                    
                }
                else
                {
                    // TODO: Do an exception here.
                    System.err.println( "No key" );
                    
                }
            }
            else if ( tokens[0].equals( "Sec-WebSocket-Version" ) ) 
            {
                IsVerSet = true;
                // TODO: Do something with the version here.
            }
            
            if ( IsKeySet && IsVerSet )
            {
                // Be EXTREMELY careful that the header ends with \r\n\r\n
                // and nothing else.  Any data after the double \r\n will
                // be processed as part of the next message received by the
                // client and will likely cause the connection to be
                // terminated.
                String msg = "HTTP/1.1 101 Switching Protocols\r\n";
                msg += "Upgrade: websocket\r\n";
                msg += "Connection: Upgrade\r\n";
                msg += "Sec-WebSocket-Accept: " + EncodedKey;
                msg += "\r\n\r\n";                
                
                try 
                { 
                    toClient.writeBytes( msg );
                    toClient.flush();
                    
                    System.out.println( "Server > Client: " + msg );
                    return true;
                    
                }
                catch ( IOException e )
                {
                    System.err.printf( "Handshake IOE: %s%n", e.getMessage() );
                    e.printStackTrace();
                    return false;
                    
                }
            }
            
            ++counter;
            
            // Handshaking should have been completed long before the counter
            // ever reaches 20.  It shouldn't go past 12 or 13 headers, based
            // on the current spec.  Bail out if we're receiving garbage data.
            if ( counter >= 20 ) return false;
            
        }

        return false;
        
    }  
    
    


    
}


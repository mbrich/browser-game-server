/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 *  DESCRIPTION:
 *  Formats messages and adds the required delimiters automatically. These 
 *  messages are then sent out by the server.
 * 
 *  USAGE:
 * 
 *      short cmdId = 10;
 *      double x_loc = 150;
 *      double y_loc = 250;
 *      
 *      ClientMsg msg = new ClientMsg( cmdId, x_loc, y_loc );
 *      If cmdId 10 was the cmd to update your player's coordinates, then this
 *      would format a message that sent your coordinates to the server.  
 *      Although the message is now formatted, it must still be sent out by the
 *      server using the SendTo class. This can be done as follows:
 * 
 *      // Sends this message to all clients connected to the server.
 *      SendTo.all( msg );
 * 
 *      // Sends this message to all clients in a session (including sender).
 *      int sessionId = 15;
 *      SendTo.session( sessionId, msg );
 *      
 *      Please see the SendTo class reference for more information on these.
 *      Also see: http://www.level65.com/docs/sendto.class.html
 */
package com.level65.GameServer.Network;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class ClientMsg 
{
    public String msg = "";
    // ASCII version, using Unicode character instead
    // so that strings can be parsed by GetEncoding( "UTF8" in C#.
    private String delimiter = "\037";
    //private String delimiter = "\u001F";
    
    
    public ClientMsg( short cmdId, int arg1 )
    {
       
        msg = Short.toString( cmdId );
        msg += delimiter;
        msg += Integer.toString( arg1 );    
        
        
    }
    
   
    
    public ClientMsg( short cmdId, int arg1, double arg2, double arg3 )
    {
        msg = Short.toString( cmdId );
        msg += delimiter;
        msg += Integer.toString( arg1 );
        msg += delimiter;
        msg += Double.toString( arg2 );
        msg += delimiter;
        msg += Double.toString( arg3 );
        
        
    }
    
    
    public ClientMsg( short cmdId, short arg1, short arg2 )
    {
        msg += Short.toString( cmdId );
        msg += delimiter;
        msg += Short.toString(arg1);
        msg += delimiter;
        msg += Short.toString(arg2);
    
    }
    
    
    public ClientMsg( short cmdId, int arg1, int arg2 )
    {

        msg = Short.toString( cmdId );
        msg += delimiter;
        msg += Integer.toString( arg1 );
        msg += delimiter;
        msg += Integer.toString( arg2 );
        
    }
    
    
    
    public ClientMsg( short cmdId, double arg1, double arg2 )
    {
        msg = Short.toString( cmdId );
        msg += delimiter;
        msg += Double.toString( arg1 );
        msg += delimiter;
        msg += Double.toString( arg2 );
        
    }
    
    
    
    public ClientMsg( short cmdId, String ... args )
    {
        msg = Short.toString( cmdId );
        
        for ( String arg : args )
        {
            msg += delimiter;
            msg += arg;
            
        } 
    }
    
    
}

/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer;

import com.level65.GameServer.Network.ClientMgr;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Michael Brich
 */
public class ServerStats
{
     static final org.slf4j.Logger LOG = 
            LoggerFactory.getLogger( ServerStats.class );
     
    private static double playerCount = 0;
    private static double threadCount = 0;
    private static double timeStarted = 0;
    
    private static final Object updateLock = new Object();
    
    
    public static double get( StatType statType )
    {
        synchronized ( updateLock )
        {
            switch ( statType )
            {
                case PLAYER_COUNT: return playerCount;
                case THREAD_COUNT: return threadCount;
                default: return (double) 0.00;
        
            }
        }
    }
    
    
    
    public static void set( StatType statType, double value )
    {
        synchronized ( updateLock )
        {

            switch ( statType )
            {
                case PLAYER_COUNT:
                    playerCount = value;
                    break;

                case THREAD_COUNT:
                    threadCount = value;
                    break;
                    
                                    
            }
        }  
    }
}

/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer;

import com.level65.GameServer.Network.ClientInputHandler;
import com.level65.GameServer.Network.ClientMgr;
import com.level65.GameServer.Network.ConnectionManager;
import com.level65.GameServer.Network.Protocol.IProtocol;
import com.level65.GameServer.Network.Protocol.TcpProtocol;
import com.level65.GameServer.Network.Protocol.WebsocketProtocol13;
import com.level65.GameServer.Commands.CommandHandler;
import com.level65.GameServer.Game.GameMgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class GameServer 
{
    static final Logger LOG = 
            LoggerFactory.getLogger(GameServer.class);
    
    public static void main( String[] args )
    {
        
        LOG.info( "[L65Server] Initializing..." );
        
        // The config object reads config.properties in the server dir and uses
        // any config values it finds. Default values are used when an expected
        // field does not exist.  Default values are also used if the config
        // file does not exist.
        ServerConfig serverConfig = new ServerConfig( args );
        ConnectionManager connectionMgr = new ConnectionManager();
        Thread connectionMgrThread = new Thread( connectionMgr );
        connectionMgrThread.start();
        
        
        Thread controlThread = new Thread( new ControlService() );
        controlThread.start();
        
        serverConfig.protocolName = "x";
        //TODO: Use serverconfig to add what protocol that is being used or not
        IProtocol protocol = null;
        if ( serverConfig.protocolName.equals( "tcpsocket" ) )
        {
            protocol = new TcpProtocol();
            
            
        }
        else
        {
            protocol = new WebsocketProtocol13();
            
        }
        
        
        LOG.debug( "Server protocol is: {}", "@@");
        
        try 
        {
            ServerSocket Server = new ServerSocket( serverConfig.port );
            
            LOG.info( "Now listening on port {}", serverConfig.port );
            LOG.info( "Server ready" );
            
            for ( ;; ) 
            {
                Socket client = Server.accept();
                
                // We have a lot of small packets that may be delayed or
                // held back due to Nagle's algorithm. Disable it.
                client.setTcpNoDelay( true );
               
                
                int nextClientId = ClientMgr.getNextClientId();
                
                ClientInputHandler clientInputHandler = 
                        new ClientInputHandler( nextClientId, client, protocol );
                

                Thread inputThread = new Thread( clientInputHandler );
                inputThread.start();

                // Register our client in the connection list.
                // We store references to the socket and to both I/O threads so
                // that we can shut down them down when we're done.
                ClientMgr.register( inputThread, client );
                

           }
        } 
        catch ( UnknownHostException e ) 
        {
            LOG.error( e.getMessage() );
            System.exit( 1 );
            
        } 
        catch ( IOException e ) 
        {
            LOG.error( e.getMessage() );
            System.exit( 1 );
            
        }
    } // ef

    
}

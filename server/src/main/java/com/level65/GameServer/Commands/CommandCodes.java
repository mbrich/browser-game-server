/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Commands;

/**
 *
 * @author Michael Brich and Kayvan Boudai
 */
public class CommandCodes 
{
   // RESERIVED BLOCK: 1 - 99
    // Basic socket actions
    /**
     * 
     */
    public final static short CLIENT_SND_MSG               = 0;
    
    /**
     * 
     */
    public final static short CLIENT_RCV_CLOSE             = 1;
    /**
     * 
     */
    
    /** Client is sending a Close Connection request.
     */
    public final static short CLIENT_SND_CLOSE             = 2;
    

    public final static short CLIENT_SND_PONG              = 3;
    

    public final static short CLIENT_RCV_PING              = 4;
    public final static short CLIENT_JOINED_SESSION       = 5;  
    public final static short CLIENT_LEFT_SESSION         = 6;

    // Javascript Event Support for entities
    // RESERVED BLOCK 100 - 199
    /**
     * 
     */
    public final static short ENTITY_SND_KEYPRESS        = 100;
    
    /** The player pressed a key on an entity.  The server will broadcast a
     *  ENTITY_RCV_KEYPRESS to nearby clients.
     */
    public final static short ENTITY_SND_KEYUP           = 101;
    
    /** Keydown event on a specific entity, sent by a specific player.  The 
     *  server will broadcast a ENTITY_RCV_KEYDOWN to all nearby clients.
     */
    public final static short ENTITY_SND_KEYDOWN         = 102;
    
    /** Player triggered an OnDragStart event on entity.  Server will send an
     *  ENTITY_RCV_DRAGSTART to nearby clients.
     */    
    public final static short ENTITY_SND_DRAGSTART       = 103;
    
    /** Player triggered an OnDragOver event (stopped dragging) on an entity. 
     *  Server will send an ENTITY_RCV_DRAGOVER event to nearby clients.
     */
    public final static short ENTITY_SND_DRAGOVER        = 104;
    
    /** Player triggered an OnFocus event for entity.  Server will send an
     *  ENTITY_RCV_FOCUS event to nearby clients.
     */
    public final static short ENTITY_SND_FOCUS           = 105;
    
    /** Player triggered an OnBlur event for entity.  Server will send an
     *  ENTITY_RCV_BLUR event to nearby clients.
     */
    public final static short ENTITY_SND_BLUR            = 106;
    
    /** Player triggered an OnClick event for entity.  Server will send an
     *  ENTITY_RCV_CLICK event to nearby clients.
     */
    public final static short ENTITY_SND_CLICK           = 107;
    
    /** Player triggered OnDblClick event for entity.  Server will send an
     * ENTITY_RCV_DBLCLICK event to nearby clients.
     */
    public final static short ENTITY_SND_DBLCLICK        = 108;
    
    /** Player triggered OnMouseOver event for entity . Server will send an
     * ENTITY_RCV_MOUSEOVER event to nearby clients.
     */
    public final static short ENTITY_SND_MOUSEOVER       = 109;
    public final static short ENTITY_SND_MOUSEOUT        = 110;
    public final static short ENTITY_SND_MOUSEWHEEL      = 111;
    
    public final static short PLAYER_RCV_KEYPRESS        = 150;
    public final static short PLAYER_RCV_KEYUP           = 151;
    public final static short PLAYER_RCV_KEYDOWN         = 152;
    public final static short PLAYER_RCV_DRAGSTART       = 153;
    public final static short PLAYER_RCV_DRAGOVER        = 154;
    public final static short PLAYER_RCV_FOCUS           = 155;
    public final static short PLAYER_RCV_BLUR            = 156;
    public final static short PLAYER_RCV_CLICK           = 157;
    public final static short PLAYER_RCV_DBLCLICK        = 158;
    public final static short PLAYER_RCV_MOUSEOVER       = 159;
    public final static short PLAYER_RCV_MOUSEOUT        = 160;
    public final static short PLAYER_RCV_MOUSEWHEEL      = 161;
    
    
    // GENERAL ENTITY UPDATES
    public final static short ENTITY_SND_POS               = 200;
    public final static short ENTITY_SND_REMOVE            = 201;
    public final static short ENTITY_SND_CREATE            = 202;
    public final static short ENTITY_SND_HP                = 203;   
    public final static short ENTITY_SND_STATS             = 204;
    
    // RESERVED BLOCK 500 - 599
    // PLAYER FUNCTIONS
    public final static short PLAYER_RCV_JOIN_SERVER       = 500;
    public final static short PLAYER_SND_LOAD_SERVER       = 501;
    public final static short PLAYER_RCV_LEAVE_SERVER      = 502;
    public final static short PLAYER_RCV_JOIN_SESSION      = 503;
    public final static short PLAYER_SND_LOAD_SESSION      = 504;
    public final static short PLAYER_RCV_HP                = 505;
    public final static short PLAYER_SND_HP                = 506;
    public final static short PLAYER_RCV_POS               = 507;
    public final static short PLAYER_SND_POS               = 508;
    public final static short PLAYER_RCV_STATS             = 509;
    public final static short PLAYER_SND_INIT              = 510;
    //public final static short PLAYER_RCV_CHANGE_LEVEL      = 511; REMOVED
    
}

/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Commands;

import com.level65.GameServer.Game.SessionMgr;
import com.level65.GameServer.Network.ClientMsg;
import com.level65.GameServer.Network.SendTo;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class CommandsGame
{
    static final Logger LOG = 
            LoggerFactory.getLogger(CommandsGame.class);
 
    public CommandsGame()
    {
       
        
        // Player Join world
        // Join World. No args
        CommandHandler.register( CommandCodes.PLAYER_RCV_JOIN_SERVER, 
                new ICommand()
        {
           public void execute( int clientId, ArrayList<String> args )
           {
               try
               {
                   System.out.println( "Creating player..." );
                   int entityId = SessionMgr.registerClient( 0, clientId );
                   SendTo.send( clientId, 
                      new ClientMsg( CommandCodes.PLAYER_SND_INIT, entityId, 
                           100, 100 ) );
                   //GameMgr.sendLevelEntities( clientId, 0 );
                   //GameHandler.getLevel( 0 ).sendNewEntities( clientId );
                   
               }
               catch ( NumberFormatException e )
               {
                   LOG.error( e.toString() );
                   //e.printStackTrace();
                   
               }
               
           }
           
           public int getArgCount() { return 0; }
            
        });   

        
        CommandHandler.register( CommandCodes.PLAYER_RCV_LEAVE_SERVER, 
                new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
            }
            
            public int getArgCount() { return 0; }
            
        } );

        CommandHandler.register( CommandCodes.PLAYER_RCV_JOIN_SESSION, 
                new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
                // Reply with a PLAYER_SND_LOAD_LEVEL or not.
                
            }
            
            public int getArgCount() { return 0; }
            
        } ); 


        CommandHandler.register( CommandCodes.PLAYER_RCV_HP, 
                new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
            }
            
            public int getArgCount() { return 0; }
            
        } ); 

        
        
        // UPDATE PLAYER POSITION
        // X, Y
        CommandHandler.register( CommandCodes.PLAYER_RCV_POS, 
                new ICommand()
        {
           public void execute( int clientId, ArrayList<String> args )
           {
               try
               {
               
                   double x = Double.parseDouble( args.get( 0 ) );
                   double y = Double.parseDouble( args.get( 1 ) );
                   
                   /*
                   GameHandler.getLevel( 
                           ClientList.getLevelId( clientId ) 
                           ).
                           updatePlayerPos( clientId, ClientList.getEntityId( clientId ), 
                           x, y );
                   */
                   
                   SessionMgr.getClientSession( clientId ).
                           updatePlayerPos( clientId, x, y );
                   
               }
               catch ( NumberFormatException e )
               {
                   
               }
               
           }
           
           public int getArgCount() { return 2; }
            
        }); 
 

        CommandHandler.register( CommandCodes.PLAYER_RCV_STATS, new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
            }
            
            public int getArgCount() { return 0; }
            
        } );     
    }
}

/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Commands;

import java.util.ArrayList;

import com.level65.GameServer.Network.ClientMgr;
import com.level65.GameServer.Network.ClientMsg;
import com.level65.GameServer.Network.SendTo;


public class CommandsInputEvents
{
   
    public CommandsInputEvents()
    {
        
        // <-------------------------------RCV-------------------------------->
        
        // Javascript Keypress Event
        CommandHandler.register( CommandCodes.PLAYER_RCV_KEYPRESS, new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_KEYPRESS,
                        keycode,
                        entityId ) );
                
                
            }
            
            public int getArgCount() { return 2; }
            
        } );
        
        // Javascript Keyup Event
        CommandHandler.register( CommandCodes.PLAYER_RCV_KEYUP, new ICommand()
        {
            /**
             * args[0] = keycode
             * args[1] = entityId
             */
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_KEYUP,
                        keycode,
                        entityId ) );
                
                
            }
            
            public int getArgCount() { return 2; }
            
            
        } );
        
        
        // Javascript Keydown Event
        CommandHandler.register( CommandCodes.PLAYER_RCV_KEYDOWN, new ICommand()
        {
            /*
             * args[0] = keycode
             * args[1] = entityId
             */
            
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_KEYDOWN,
                        keycode,
                        entityId ) );
                
                
            }
            
            public int getArgCount() { return 2; }
            
        } );
        
        CommandHandler.register( CommandCodes.PLAYER_RCV_DRAGSTART, new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_DRAGSTART,
                        keycode,
                        entityId ) );
                
                
            }
            
            public int getArgCount() { return 0; }
            
        } );
        
        CommandHandler.register( CommandCodes.PLAYER_RCV_DRAGOVER, new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_DRAGOVER,
                        keycode,
                        entityId ) );
                
                
            }
            
            public int getArgCount() { return 0; }
            
        } );
        
        // Javascript onFocus Event
        CommandHandler.register( CommandCodes.PLAYER_RCV_FOCUS, new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_FOCUS,
                        keycode,
                        entityId ) );
                
                
            }
            
            public int getArgCount() { return 0; }
            
        } ); 
        
        // Javascript onBlur Event
        CommandHandler.register( CommandCodes.PLAYER_RCV_BLUR, new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_BLUR,
                        keycode,
                        entityId ) );
                
                
            }
            
            public int getArgCount() { return 0; }
            
        } );   
        
        // Javascript onClick Event
        CommandHandler.register( CommandCodes.PLAYER_RCV_CLICK, new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_CLICK,
                        keycode,
                        entityId ) );
                
                
            }
                       
            public int getArgCount() { return 0; }
            
        } );   
        
        
        // Javascript onDblClick event
        CommandHandler.register( CommandCodes.PLAYER_RCV_DBLCLICK, new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_DBLCLICK,
                        keycode,
                        entityId ) );
                
                
            }
                         
            public int getArgCount() { return 0; }
            
        } );
        
        CommandHandler.register( CommandCodes.PLAYER_RCV_MOUSEOVER, new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_MOUSEOVER,
                        keycode,
                        entityId ) );
                
                
            }
            
            public int getArgCount() { return 0; }
            
        } );
        
        CommandHandler.register( CommandCodes.PLAYER_RCV_MOUSEOUT, new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_MOUSEOUT,
                        keycode,
                        entityId ) );
                
                
            }
            
            public int getArgCount() { return 0; }
            
        } );
        
        CommandHandler.register( CommandCodes.PLAYER_RCV_MOUSEWHEEL, new ICommand()
        {
            public void execute( int clientId, ArrayList<String> args )
            {
                String keycode = args.get(0).toString();
                String entityId = args.get(1).toString();
                
                
                SendTo.mySession( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_MOUSEWHEEL,
                        keycode,
                        entityId ) );
                
                
            }
            
            public int getArgCount() { return 0; }
            
        } );
        
        
    }
}

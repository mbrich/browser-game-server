/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Commands;

import com.level65.GameServer.Exceptions.CommandExistsException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class CommandHandler 
{
    static final Logger LOG = 
            LoggerFactory.getLogger(CommandHandler.class);    
    
    public static HashMap<Short,ICommand> commandMap 
            = new HashMap<Short,ICommand>();
    
    // Private to prevent instantiation. This class is intended to be static.
    private CommandHandler() { }

    
    public static void register( Short cmdId, ICommand cmd )
    {
        try
        {
            if ( commandMap.containsKey( cmdId ) )
            {
                commandMap.put( cmdId, cmd );
                LOG.trace( "Command {} registered", cmdId );
                // TODO: Place a real and useful error message here.
                throw new CommandExistsException( "Something" );
                
                
            }
        }
        catch ( CommandExistsException e )
        {
            e.printStackTrace();

        }       
    }
  
    
    /**
     * Checks the commandMap to see whether this command has been loaded into it
     * already.
     * @param commandName
     * @return
     */
    public static boolean hasCommand( short commandName )
    {
        if ( commandMap.containsKey( commandName ) )
        {
            return true;
    
        }
        else
        {
            return false;
            
        }
    } // ef
    
    
    /**
     * Calls the specified command and passes the list of args to the specified
     * method.
     * @param clientId
     * @param commandName
     * @param args
     * @return 
     */
    public static boolean doCommand( int clientId, short commandName, 
            ArrayList<String> args )
    {
        LOG.debug( "Trying command: {}", commandName );
        
        if ( hasCommand( commandName ) )
        {
            if ( ( commandMap.get( commandName ) ).getArgCount() >= args.size() - 1 )
            {    
                ( commandMap.get( commandName ) ).execute( clientId, args );
                return true;
            
            }
            else
            {
                LOG.warn( "Invalid argcount for {}", commandName );
                return false;
                
            }
        }
        else
        {
            LOG.warn( "No such command: {}", commandName );
            return false;
            
        }
    } // ef

}


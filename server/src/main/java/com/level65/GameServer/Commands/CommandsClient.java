/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Commands;


import com.level65.GameServer.Network.ClientInfo;
import com.level65.GameServer.Network.ClientMsg;
import com.level65.GameServer.Network.SendTo;
import java.util.ArrayList;

/**
 *
 * 
 */
public class CommandsClient
{
  
    
    public CommandsClient()
    {
        
        
        // Message to Client
        // MTC|TargetClientId|MSG
        CommandHandler.register( CommandCodes.CLIENT_SND_MSG, new ICommand()
        {	
            public void execute( int clientId, ArrayList<String> args )
            {
                System.out.println( "Message to Client: " + args.get(0) );
                
                SendTo.send( clientId, new ClientMsg(
                        CommandCodes.CLIENT_SND_MSG, 
                        args.get(0) ) );
                
            }
            
            public int getArgCount() { return 2; }
            
        });

        CommandHandler.register( CommandCodes.CLIENT_SND_CLOSE, new ICommand()
        {	
            public void execute( int clientId, ArrayList<String> args )
            {
                System.out.println( "closing" );
                SendTo.send( clientId, 
                        new ClientMsg( CommandCodes.CLIENT_RCV_CLOSE ) );
                
            }
            
            public int getArgCount() { return 2; }
            
        });
    }
    
    
    
    
}

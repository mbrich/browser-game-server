/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loads config keys from a file (if present) and validates each specified
 * key.  Invalid/Missing keys are ignored default values are used instead.
 * 
 * @author Kayvan Boudai and Michael Brich
 */
public class ServerConfig 
{
    
    public int port = 8181;
    public String configFileDir = "./";
    public String configFileName = "config.properties";
    public String protocolName = "Websocket13";
    public int maxUsers = 0;
    private static final org.slf4j.Logger LOG = 
            LoggerFactory.getLogger( ServerConfig.class );
    
    public ServerConfig( String [] args )
    {
        // TODO: Command line argument parser to allow config filename
        // and dir to be specified.
    	
        String configFilePath = this.configFileDir + this.configFileDir;
        ///System.out.printf( "[BGServer] Looking for config file: %s%n", 
        //                 configFilePath );
        
        Properties properties = new Properties();
        boolean UseConfigFile = false;
        
        try 
        {
            
            FileInputStream fileStream = new FileInputStream( configFilePath );
            properties.load( fileStream );
            fileStream.close();
            UseConfigFile = true;
            
        }
        catch ( FileNotFoundException e )
        {
            LOG.error( "Config.properties file was not found in the server's" +
                    " directory." );
            
        } 
        catch ( SecurityException e )
        {
           LOG.error( "Config.properties file found, but the server does not " +
                   "have access to read it" );
        
        }
        catch ( IOException e )
        {
            e.printStackTrace();
           
        }
        
        
        if ( UseConfigFile == true )
        {
            LOG.info( "Config.properties file found.  Loading data..." );
            
            if ( properties.getProperty( "port" ) != null )
            {
                this.setPort( properties.getProperty( "port" ) );
                
            }
            
            
        }
        

        //after the config file is loaded check if there are commandline args
        //that should overwrite the config files
        if (args.length > 0)
    	{
            for(String arg: args)
            {
                arg = parseDash(arg);

                switch (arg.charAt(0))
                {
                    case 'p':
                        arg = splitArg(arg);
                        if (arg != null)
                        {
                            setPort(arg);
                        }
                        else
                        {
                            System.err.println( "[BGServer] Invalid argument!" );
                        }
                        break;
                    
                    case 'r':
                        if ( arg != null )
                        {
                            setProtocol( arg );
                            
                        }
                        break;
                       
                    case 'm':
                        arg = splitArg(arg);
                        if (arg != null)
                        {
                            setMaxUsers(arg);
                        }
                        else
                        {
                            System.err.println( "[BGServer] Invalid argument!" );
                        }
                        break;

                    default:
                        System.err.println( "[BGServer] Invalid argument!" );
                        break;
                }
            }
    	}
    } // ef
    
    public void setProtocol( String _protocolName )
    {
        this.protocolName = _protocolName;
    }
    
    /**
     * Set the server's listening port. If there are any problems parsing 
     * the string to an int, the user is warned and the default port is used
     * instead.
     * @param portNum
     */
    public void setPort( String portNum )
    {
        
        try 
        {
            int portInt = Integer.parseInt( portNum );
            this.setPort( portInt );
            
        }
        catch ( NumberFormatException e )
        {
            System.err.println( "[BGServer] Invalid port value specified in "
                    + "the server settings file.  This value could not be"
                    + "interpreted as a valid number. Default port will be used" );
            
        }
    } // ef
    
    
    
    /**
     * Set the server's listening port. Attempts to validate and warn the user
     * if the port number is invalid.  Default port number is used if this
     * fails.
     * @param portNum
     */
    public void setPort( int portNum )
    {
        if ( portNum > 65535 || portNum < 1 )
        {
            System.err.println( "[BGServer] Invalid port number specified in"
                    + "the server settings file.  Please select a port number"
                    + "between 1 and 65535" );
            
        }
        else
        {
            this.port = portNum;  
            
        }
    } // ef
    
    
    
    /**
     * Set the max amount of users allowed in this server. If there is an
     * error the user is warned that there will be no limit.
     * @param max
     */
    public void setMaxUsers(String maxNum)
    {
        try 
        {
            int maxInt = Integer.parseInt( maxNum );
            this.setMaxUsers( maxInt );
            
        }
        catch ( NumberFormatException e )
        {
            System.err.println( "[BGServer] Invalid max user value specified in "
                    + "the server settings file.  This value could not be"
                    + "interpreted as a valid number. Default limit used. " );
            
        }
    }
    
    /**
     * Set the max amount of users allowed in this server.
     * @param max
     */
    public void setMaxUsers(int maxNum)
    {
    	this.maxUsers = maxNum;
    	System.out.println("[BGServer] Set max user limit to " + this.maxUsers);
    }
    
    
    public String parseDash(String flag)
    {
    	if(flag.charAt(0) == '-')
    	{
            return flag.substring(1);
    	}
    	else
    	{
            return flag;
    	}
    }
    
    public String splitArg(String arg)
    {
    	String [] argSplit = arg.split(":");
	if ( argSplit.length == 2 )
	{
            return argSplit[1];
	}
	else
	{
            return null;
	
        }
    }
    
}

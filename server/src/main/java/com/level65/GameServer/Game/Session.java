/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Game;

import com.level65.GameServer.Commands.CommandCodes;
import com.level65.GameServer.Network.ClientMgr;
import com.level65.GameServer.Network.ClientMsg;
import com.level65.GameServer.Network.SendTo;
import java.util.ArrayList;

/**
 * @author Michael Brich
 */
public class Session 
{
    public int sessionId = 0;
    public int playerCount = 0;
    public int maxPlayers = 0;
    public String password = "";
    public ArrayList<Entity> entities = new ArrayList<Entity>();
    public ArrayList<Integer> clients = new ArrayList<Integer>();
    public SessionState state = SessionState.LOBBY;
    
    public Session( int _sessionId, 
            String _password, 
            int _maxPlayers )
    {
        this.maxPlayers = _maxPlayers;
        
    }
    
    /**
     * Creates an entity inside a game session.
     * @param entity
     * @return 
     */
    public int createEntity( Entity entity )
    {
        synchronized( this.entities )
        {
            this.entities.add( entity );
            return this.entities.size() - 1;
            
        }
    }
    
    
    public int createClient( int clientId, Entity entity )
    {
        int entityId = this.createEntity( entity );
        synchronized( this.clients )
        {
            // This list gives a quick way to find the entityId for all
            // clients in the area.
            this.clients.add( entityId, clientId );
        
        }
        
        return entityId;
        
    }
    
    /**
     * Removes an entity from a game.
     * @param entityId 
     */
    public void removeEntity( int entityId )
    {
        synchronized ( this.entities )
        {
            this.entities.remove( entityId );
        
        }
    }
    
    
    public void removeClient( int clientId )
    {
        
        synchronized ( this.clients )
        {
           
        }
    }
    
    public Entity getEntity( int entityId )
    {
        // TODO: Proper safety handling for non-existant records.
        return this.entities.get( entityId );
        
    }
    /**
     * Updates the position for a specific client.
     * @param clientId
     * @param x
     * @param y 
     */
    public void updatePlayerPos( int clientId, double x, double y )
    {
        int sessionId = ClientMgr.getSessionId(clientId);
        
        int entityId = ClientMgr.getEntityId( clientId );
        updateEntityPos( sessionId, entityId, x, y );
        SendTo.mySession( clientId, 
                new ClientMsg( CommandCodes.ENTITY_SND_POS, x, y ) );
        
        
    }
    /**
     * Creates the entity and notifies players in the level that it exists.
     * @param levelId
     * @param entity
     * @return 
     */
    public int registerEntity( int levelId, Entity entity )
    {
        return -1;
        
    }
    
    
    public void sendLevelEntities( int clientId, int levelId )
    {
        
        int myEntityId = ClientMgr.getEntityId( clientId );
        int entityId = 0;
        for ( Entity entity : this.entities )
        {
            if ( entity != null && entityId != myEntityId )
            {
                SendTo.send( clientId, 
                        new ClientMsg( CommandCodes.ENTITY_SND_CREATE, 
                        entityId,
                        entity.position.x,
                        entity.position.y
                        ) );

            }

            ++entityId;

        }  
    }   
    
    public static void joinWorld( int clientId )
    {
      
        
    }
    

    
    
    /**
     * Registers a player player's entity within a specific chunk and notifies
     * players within that chunk to create the new player's entity.
     * @param entityId 
     */
    public static void registerPlayer( int sessionId, int clientId )
    {
        SendTo.mySession(clientId, 
                new ClientMsg( CommandCodes.CLIENT_JOINED_SESSION, clientId ) );
        
    }
    
    /**
     * Registers a player with a specific chunk and notifies players within
     * the chunk that the player's entity should be removed.
     * @param chunkId
     * @param clientId 
     */
    public static void unregisterPlayer( int sessionId, int clientId )
    {
         SendTo.mySession(clientId, 
                new ClientMsg( CommandCodes.CLIENT_LEFT_SESSION, clientId ) );       
        
    }
    
    

    
    /**
     * Update the position for an entity.
     * @param levelId
     * @param entityId
     * @param x
     * @param y 
     */
    public void updateEntityPos( int levelId, int entityId, double x,
            double y)
    {
        
        synchronized ( this.entities )
        {
            Entity entity = getEntity( entityId );
            synchronized ( entity )
            {
                entity.position.x = x;
                entity.position.y = y;
            }
        }
            
        
    }      
}

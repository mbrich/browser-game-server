/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Game;

import com.level65.GameServer.Network.ClientMgr;
import com.level65.GameServer.Network.ClientMsg;
import com.level65.GameServer.Network.SendTo;
import com.level65.GameServer.Commands.CommandCodes;
import java.util.ArrayList;


/**
 *
 * @author Kayvan Boudai and Michael Brich
 */

public class GameMgr 
{

    public static void sendSessionEntities()
    {
    
    }
    
    
    
    /*
    public static int joinLevel( int clientId, int levelId )
    {
        Entity player = new Entity();
        
        // TODO: Case for -1 returned (error).
        int entityId = registerLevelPlayer( clientId, levelId, player );
        ClientMgr.setLevelId( clientId, levelId );
        ClientMgr.setEntityId( clientId, entityId );
        return entityId;
        
    }
    */
    
    
    
    /*
    public static int registerLevelPlayer( int clientId, int levelId, Entity player )
    {
        if ( levelList.get( levelId ) != null )
        {
            int entityId = levelList.get( levelId ).createPlayer( 
                    clientId, player );
            
            SendTo.mySession( clientId, 
                new ClientMsg( CommandCodes.ENTITY_SND_CREATE, 
                    entityId, 
                    player.position.x,
                    player.position.y 
                    ) );          
            
            ClientMgr.setEntityId( clientId, entityId );
            
            return entityId;
            
        }
        else
        {
            return -1;
            
        }  
    }
    */
    
    
    /**
     * Removes the player's entity from a level and notifies nearby players
     * to delete the entity.
     * @param clientId
     * @param levelId 
     */
    public static void unregisterLevelPlayer( int clientId, int levelId )
    {
        int entityId = ClientMgr.getEntityId( clientId );
               
        //this.notifyLevelEntityRemoved( sourceClientId, entityId );
        //ClientSender.notifyMyLevel( )
        
    }    
    
    

    
    
}
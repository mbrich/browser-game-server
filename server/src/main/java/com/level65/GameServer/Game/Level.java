/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Game;

import com.level65.GameServer.Commands.CommandCodes;
import com.level65.GameServer.Network.ClientMsg;
import com.level65.GameServer.Network.ClientMgr;
import com.level65.GameServer.Network.SendTo;
import java.util.ArrayList;

/**
 * @author Kayvan Boudai and Michael Brich
 */
public class Level 
{
    
    private ArrayList<Entity> entities = new ArrayList<Entity>();
    private ArrayList<Integer> players = new ArrayList<Integer>();
    
    
    
    
    
    public ArrayList<Entity> getEntities()
    {
        return this.entities;
    
    }
    
    public ArrayList<Integer> getClients()
    {
        // TODO: Change later
        return this.players;
    
    }
    
    
    
    public ArrayList<Integer> getPlayers()
    {
        return this.players;
        
    }
    
    
    public Entity getEntity( int entityId )
    {
        // TODO: Proper safety handling for non-existant records.
        return this.entities.get( entityId );
        
    }
    
    
    public int createPlayer( int clientId, Entity entity )
    {
        int entityId = this.createEntity( entity );
        synchronized( this.players )
        {
            // This list gives a quick way to find the entityId for all
            // clients in the area.
            this.players.add( entityId, clientId );
        
        }
    
        return entityId;
        
    }
    
    
    public void sendNewEntities( int clientId )
    {
    	// TODO: this and sendFromPlayer are assigned
    	//			to the same command code?!?!
    	System.out.println("sendNewEntities create this entity with this pos");
        
        int myEntityId = ClientMgr.getEntityId( clientId );
        for ( Entity entity : this.entities )
        {
            
            int entityId = this.entities.indexOf( entity );
            if ( entityId != myEntityId )
            {
                
                SendTo.send( clientId, new ClientMsg(
                        CommandCodes.ENTITY_SND_CREATE,
                        entity.position.x,
                        entity.position.y 
                        ) );
                
            }
        }

    }
     
     
    public int createEntity( Entity entity )
    {
        synchronized( this.entities )
        {
            this.entities.add( entity );
            return this.entities.size() - 1;
            
        }
    }
    
    public void removeEntity( int entityId )
    {
        this.entities.remove( entityId );
    
    }

    
    
}

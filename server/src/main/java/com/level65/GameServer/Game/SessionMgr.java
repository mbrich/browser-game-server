/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer.Game;

import com.level65.GameServer.Commands.CommandCodes;
import com.level65.GameServer.Network.ClientMgr;
import com.level65.GameServer.Network.ClientMsg;
import com.level65.GameServer.Network.SendTo;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Michael Brich
 */
public class SessionMgr 
{
    private final static ArrayList<Session> sessions =
            new ArrayList<Session>();
    private static int nextSessionId = 0;
    private static Object nextIdLock = new Object();
    static final Logger LOG = 
            LoggerFactory.getLogger(SessionMgr.class);    
    // Private Constructor to prevent instantiation. There should be no
    // instances of this class.
    private SessionMgr() {}
    /**
     * Creates a game session with the specified parameters.
     */
    public static void create()
    {
        synchronized ( sessions )
        {
            synchronized ( nextIdLock )
            {
                sessions.add( new Session( nextSessionId, "", 8 ) );
                // Create the session
                ++nextSessionId;
            }
        }
    }
    

  
    public static ArrayList<Integer> clients( int sessionId )
    {
        Session session = get( sessionId );
        synchronized ( session.clients )
        {
            return session.clients;
            
        }
        
    }
    /**
     * Performs a lookup on the client to see what session they are currently
     * in, then returns that session.
     * @param clientId
     * @return 
     */
    public static Session getClientSession( int clientId )
    {
        int sessionId = ClientMgr.getSessionId(clientId);
        return get( sessionId );
        
    }
    
    /**
     * Returns the specified session.
     * @param sessionId
     * @return 
     */
    public static Session get( int sessionId )
    {
        synchronized( sessions )
        {
            Session session = sessions.get( sessionId );
            
            return session;
            
        }
        
    }
    
    /**
     * Convenience method to return all sessions on the server, including both
     * games and lobbies.
     * @return 
     */
    public static ArrayList<Session> allSessions()
    {
        return allSessions( SessionState.DEFAULT );
        
    }
    
    /**
     * Get a list of all sessions on the server, including games and lobbies.
     * @return 
     */
    public static ArrayList<Session> allSessions( SessionState state )
    {
        ArrayList<Session> tmp = new ArrayList<Session>();
        
        for ( Session session : sessions )
        {
            if ( state == SessionState.DEFAULT )
            {
                tmp.add( session );
            
            }
            else
            {
                if ( session.state == state ) tmp.add( session );
                
            }
        }
        
        return tmp;
        
    }
    
    /**
     * Convenience method to return all lobbies (no games), using the
     * allSessions method.
     * @return 
     */
    public static ArrayList<Session> allLobbies()
    {
        return allSessions( SessionState.LOBBY );
        
    }
    
    /**
     * Convenience method to return all games (no lobbies), using the
     * allSessions method.
     * @return 
     */
    public static ArrayList<Session> allGames()
    {
        return allSessions( SessionState.GAME );
    }
    
    
    /**
     * Delete a game session.
     * @param sessionId 
     */
    public static void delete( int sessionId )
    {
        synchronized ( sessions )
        {
            sessions.remove( sessionId );
            // TODO: May need to do notifications to clients.
            
        }
    }
    
    
    /**
     * Registers a client with a given session.
     * @param sessionid
     * @param clientId 
     */
    public static int registerClient( int sessionId, int clientId )
    {
        Session session = get( sessionId );
        return session.createClient(clientId, new Entity());
        
    }
    
    
    /**
     * Unregisters a client from a given session.
     * @param sessionId
     * @param clientId 
     */
    public static void unregisterClient( int sessionId, int clientId )
    {
        
    }
    
    
    /**
     * Updates the password on a game session.  This only lobbies when a session
     * is still a lobby.  Once the lobby becomes a game it can no longer be
     * joined and the password is irrelevant.
     * @param sessionId
     * @param password 
     */
    public static void updatePassword( int sessionId, String password )
    {
        Session session = sessions.get( sessionId );
        
        synchronized ( session )
        {
            session.password = password;
            // TODO: May need to notify clients.
            
        }
    }
    

}

/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.level65.GameServer;

/**
 *
 * @author Michael Brich
 */
// We use the StatType enum here instead of a string because
// older enterprise Linux distros (RHEL/CENTOS 5/6) don't support
// Java SE 7, which added support for switching on strings.
// In the future this might get changed, but right now those
// versions of CentOS/RHEL are still in fairly common use.
public enum StatType
{
    PLAYER_COUNT,
    THREAD_COUNT
    
}
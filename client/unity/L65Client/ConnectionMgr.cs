﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;



public class ConnectionMgr : MonoBehaviour 
{
	
	// Use this for initialization
	L65Connector connector;
	
	void Start () 
	{
		Debug.Log ( "Starting connection..." );
		L65Connector.Connect ();
		
	}
	
	void OnApplicationQuit()
	{
		Debug.Log ( "AppQuit" );
		L65Connector.Disconnect ();
		
	}	
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}



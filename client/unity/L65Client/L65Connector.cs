﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;



public class L65Connector : MonoBehaviour 
{
	private static String response = String.Empty;	
	public static ManualResetEvent connectDone
		= new ManualResetEvent( false );
	
	public static ConnectorStatus status = 
		ConnectorStatus.Disconnected;
	private static Timer ConnectTimer;
	private static Socket client;
	
	private L65Connector()
	{
	}
	
	
	
	private static void OnConnectTimer( object obj )
	{
		ConnectTimer.Dispose();
		Debug.Log ( "Timer reached" );
		if  ( !client.Connected )
		{
			Debug.Log ( "Socket not yet open. Aborting all. ");
			client.Close ();
			status = ConnectorStatus.Disconnected;
			
		}
		else
		{
			Debug.Log ( "Socket open, we're done here." );
			status = ConnectorStatus.Connected;
			
		}
		
		
		
	}
	
	public static void Connect()
	{
		int port = 8181;
		try
		{
			IPAddress ipAddress = IPAddress.Parse ("127.0.0.1" );
			IPEndPoint remoteEP = new IPEndPoint( ipAddress, port );
			
			client = new Socket( AddressFamily.InterNetwork,
				SocketType.Stream, ProtocolType.Tcp );
			
			status = ConnectorStatus.Connecting;
		
			
		    client.BeginConnect(remoteEP, 
		        new AsyncCallback(ConnectCallback), client );
			
			
			
			ConnectTimer = new Timer( OnConnectTimer, null, 1000, 
				Timeout.Infinite );
			
   	
		}
		catch ( Exception e )
		{
			Debug.Log ( e.ToString() );
			
		}
			
		
	}
	

	
	private static void Receive( Socket client ) 
	{
	    try 
		{
	        // Create the state object.
	        StateObject state = new StateObject();
	        state.workSocket = client;
			Debug.Log ( "Receive Called" );
	        // Begin receiving the data from the remote device.
	        client.BeginReceive( state.buffer, 
				0, 
				StateObject.BufferSize, 
				0,
	            new AsyncCallback( OnReceiveCallback ), 
				state );
			
	    } 
		catch ( Exception e ) 
		{
	        Console.WriteLine(e.ToString());
	    
		}
	}
	
	
	
    private static void OnReceiveCallback( IAsyncResult ar ) 
	{
        Debug.Log ( "Data received" );
		try 
		{
            // Retrieve the state object and the client socket 
            // from the asynchronous state object.
            StateObject state = (StateObject) ar.AsyncState;
            Socket client = state.workSocket;

            // Read data from the remote device.
            int bytesRead = client.EndReceive(ar);
			
			char delim = (char) 0000;
			
            if ( bytesRead > 0 ) 
			{
            	Debug.Log ( "BytesRead: " + bytesRead.ToString() );
				// There might be more data, so store the data received so far.
            	state.sb.Append( Encoding.UTF8.GetString( state.buffer, 0, bytesRead ) );
				
				string tmp = Encoding.UTF8.GetString ( state.buffer, 0, bytesRead );
				
				Debug.Log ( tmp );
				
				if ( tmp.IndexOf( delim.ToString() ) != -1 )
				{
					Debug.Log ( "EOF");	
				}
				
                // Get the rest of the data.
                client.BeginReceive(state.buffer,0,StateObject.BufferSize,0,
                    new AsyncCallback( OnReceiveCallback ), state );
    			
				//Debug.Log ( "Buffer: " + state.sb );
				
			} 
			else 
			{
                // All the data has arrived; put it in response.
                if ( state.sb.Length > 1 ) 
				{
                    response = state.sb.ToString();
                	Debug.Log ( "Response: " + response );
				
				}
                // Signal that all bytes have been received.
               // receiveDone.Set();
            }
        } 
		catch ( Exception e ) 
		{
            Debug.Log(e.ToString());
        }
    }	
	
	
	
	private static void Send( Socket client, String data ) 
	{
	    // Convert the string data to byte data using ASCII encoding.
	    byte[] byteData = Encoding.ASCII.GetBytes(data);
	
	    // Begin sending the data to the remote device.
	    client.BeginSend(byteData, 0, byteData.Length, SocketFlags.None,
	        new AsyncCallback(SendCallback), client);
	}	
	
	
	
	private static void SendCallback( IAsyncResult ar ) 
	{
	    try 
		{
	        // Retrieve the socket from the state object.
	        Socket client = (Socket) ar.AsyncState;
	
	        // Complete sending the data to the remote device.
	        int bytesSent = client.EndSend(ar);
	        Console.WriteLine("Sent {0} bytes to server.", bytesSent);
	
	        // Signal that all bytes have been sent.
	        //sendDone.Set();
	    } 
		catch ( Exception e ) 
		{
	        Console.WriteLine(e.ToString());
	    
		}
	}	
	
	
	
	private static void ConnectCallback( IAsyncResult ar )
	{
		Socket client = (Socket) ar.AsyncState;
		client.EndConnect( ar );
		connectDone.Set ();
		Debug.Log ( "Socket connected" );
		status = ConnectorStatus.Connected;
		Receive ( client );
		
	}
	
	/**
	 * 
	 */
	public static void Disconnect()
	{
		client.Close ();	
		status = ConnectorStatus.Disconnected;
		
	}
	


}

public enum ConnectorStatus
{
	Disconnected,
	Connecting,
	Connected
	
}

public class StateObject 
{
    // Client socket.
    public Socket workSocket = null;
    // Size of receive buffer.
    public const int BufferSize = 256;
    // Receive buffer.
    public byte[] buffer = new byte[BufferSize];
    // Received data string.
    public StringBuilder sb = new StringBuilder();
}


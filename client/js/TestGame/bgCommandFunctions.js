var DefaultCmds = 
{
	// 0 - 99
	// Basic socket actions 
	messageToClent 	: function()
	{
		bgSocket.sendMsg(arguments[0]);
	},

	rcvCloseClient : function()
	{
		//Closing handshake
		if (bgSocket.WebSocket.readyState != socketState.CLOSING &&
			bgSocket.WebSocket.readyState != socketState.CLOSED )
		{
			console.log('[BGClient] Closing...');
			bgSocket.WebSocket.readyState = socketState.CLOSING;
			bgSocket.WebSocket.close(1000, "Closing correctly through Command");
			ig.game.getEntitiesByType( EntityPlayer )[0].kill();
		}
	},
	sndCloseClient : function()
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndCloseClient );
	},

	sndPong : function()
	{

	},

	rcvPing : function()
	{

	},


	// 100 - 999
	// JavaScript Event Support for entities
	// args entityId, keycode
	rcvEntityKeypress : function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('keyPress', cmd[2]);
	},

	rcvEntityKeyup	: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('keyUp', cmd[2]);
	},

	rcvEntityKeydown: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('keyDown', cmd[2]);
	},

	rcvEntityDragstart: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('dragStart', cmd[2]);
	},

	rcvEntityDragover: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('dragOver', cmd[2]);
	},

	rcvEntityFocus	: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('focus', cmd[2]);
	},

	rcvEntityBlur	: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('blur', cmd[2]);
	},

	rcvEntityClick	: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('click', cmd[2]);
	},

	rcvEntityDblClick: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('dblClick', cmd[2]);
	},

	rcvEntityMouseover: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('mouseOver', cmd[2]);
	},

	rcvEntityMouseout: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('mouseOver', cmd[2]);
	},

	rcvEntityMousewheel: function(cmd)
	{
		bgClient.entityList[cmd[1]].virtualKeyboard.
		doEvent('mouseWheel', cmd[2]);
	},


	// JavaScript Event Support for player
	// arg keyCode
	sndPlayerKeypress: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerKeypress, entityId, keyCode);
	},

	sndPlayerKeyup	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerKeyup, entityId, keyCode);
	},

	sndPlayerKeydown: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerKeydown, entityId, keyCode);
	},

	sndPlayerDragstart: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerDragstart, entityId, keyCode);
	},

	sndPlayerDragover: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerDragover, entityId, keyCode);
	},

	sndPlayerFocus	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerFocus, entityId, keyCode);
	},

	sndPlayerBlur	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerBlur, entityId, keyCode);
	},

	sndPlayerClick	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerClick, entityId, keyCode);
	},

	sndPlayerDblClick: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerDblClick, entityId, keyCode);
	},

	sndPlayerMouseover: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerMouseover, entityId, keyCode);
	},

	sndPlayerMouseout: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerMouseout, entityId, keyCode);
	},

	sndPlayerMousewheel: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerMousewheel, entityId, keyCode);
	},

	// General Entity Updates
	rcvEntityPos	: function()
	{

	},

	rcvRemoveEntity	: function(cmd)
	{
		for (var i = 0; i < ig.game.entities.length; i++)
		{
		 	if ( ig.game.entities[i].EntityID == cmd[1] )
		 	{
		 		ig.game.entities[i].kill();
		 		break;
		 	}
		}
	},

	rcvCreateEntity	: function( cmd )// [Cmd, EntityID, X, Y]
	{
		bgClient.entityList[cmd[1]] = 
		ig.game.spawnEntity(EntityPlayer2, 152, 88, {EntityID : cmd[1]});

	},

	rcvEntityHP		: function()
	{

	},

	sndEntityStats	: function()
	{

	},

	// 500 - 599
	// Player Functions
	sndJoinWorld	: function()
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndJoinWorld );
	},

	rcvLoadWorld	: function()
	{

	},

	sndleaveWorld	: function()
	{
		//Close socket and other closing events

	},

	sndJoinLevel	: function()
	{

	},

	rcvLoadLevel	: function()
	{

	},

	sndPlayerHP		: function()
	{

	},

	rcvPlayerHP		: function()
	{

	},

	sndPlayerPos	: function()  // [x, y, socket]
	{
		bgSocket.sendCmd(	DefaultCmdCodes.sndPlayerPos,
							arguments[0], arguments[1] );
	},

	rcvPlayerPos	: function()
	{

	},

	sndPlayerStats	: function()
	{

	},

	rcvInitPlayer	: function(cmd) // [Cmd, EntityID, X, Y]
	{
		bgClient.entityList[cmd[1]] = 
		ig.game.spawnEntity(EntityPlayer, 152, 88, {EntityID : cmd[1]});
	},

	sndchangeLevel	: function()
	{

	}

	// 2000 - 32768
	// Developer cmds
	
};


var DefaultCmdCodes =
{
	// 0 - 99
	// Basic socket actions 
	messageToClent 			: 0,
	rcvCloseClient			: 1,
	sndCloseClient			: 2,
	sndPong					: 3,
	rcvPing					: 4,

	// 100 - 999
	// JavaScript Event Support for entities
	rcvEntityKeypress 		: 100,
	rcvEntityKeyup			: 101,
	rcvEntityKeydown		: 102,
	rcvEntityDragstart		: 103,
	rcvEntityDragover		: 104,
	rcvEntityFocus			: 105,
	rcvEntityBlur			: 106,
	rcvEntityClick			: 107,
	rcvEntityDblClick		: 108,
	rcvEntityMouseover		: 109,
	rcvEntityMouseout		: 110,
	rcvEntityMousewheel 	: 111,

	sndPlayerKeypress		: 150, 
	sndPlayerKeyup			: 151, 
	sndPlayerKeydown		: 152, 
	sndPlayerDragstart		: 153, 
	sndPlayerDragover		: 154,
	sndPlayerFocus			: 155,
	sndPlayerBlur			: 156, 
	sndPlayerClick			: 157, 
	sndPlayerDblClick		: 158, 
	sndPlayerMouseover		: 159, 
	sndPlayerMouseout		: 160,
	sndPlayerMousewheel		: 161,

	// General Entity Updates
	rcvEntityPos			: 200,
	rcvRemoveEntity			: 201,
	rcvCreateEntity			: 202,
	rcvEntityHP				: 203,
	sndEntityStats			: 204,
	
	// 500 - 599
	// Player Functions
	sndJoinWorld			: 500,
	rcvLoadWorld			: 501,
	sndleaveWorld			: 502,
	sndJoinLevel			: 503,
	rcvLoadLevel			: 504,
	sndPlayerHP				: 505,
	rcvPlayerHP				: 506,
	sndPlayerPos			: 507,
	rcvPlayerPos			: 508,
	sndPlayerStats			: 509,
	rcvInitPlayer			: 510,
	sndchangeLevel			: 511

	// 2000 - 32768
	// Developer cmds


};








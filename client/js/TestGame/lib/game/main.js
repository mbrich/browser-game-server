ig.module( 
	'game.main' 
)
.requires(
	'impact.game',
	'impact.font',

	'plugins.bgPlugin',

	'game.entities.player',
	'game.entities.player2',

	'game.levels.test',

	'impact.debug.debug'
)
.defines(function(){

MyGame = ig.Game.extend({
	// Load a font
	font: new ig.Font( 'media/04b03.font.png' ),
	bgsClient : null,
	stopUpdate: false,
	
	init : function() 
	{
		ig.input.bind( ig.KEY.UP_ARROW, 'up');
		ig.input.bind( ig.KEY.DOWN_ARROW, 'down');
		ig.input.bind( ig.KEY.LEFT_ARROW, 'left');
		ig.input.bind( ig.KEY.RIGHT_ARROW, 'right');
		ig.input.bind( ig.KEY.SPACE , 'space');
		ig.input.bind( ig.KEY.A , 'a');

		this.clearColor = "white";

		this.loadLevel( LevelTest );
		
		this.bgsClient = new ig.Bgplugin();
	},
	
	update: function() 
	{
		this.parent();
		if(!this.stopUpdate) this.bgsClient.update();
		
		if ( ig.input.pressed('space'))
		{
			this.stopUpdate = !(this.stopUpdate);

		}
		
		// Add your own, additional update code here
	},
	
	draw: function() 
	{
		this.parent();

	}
});


// Start the Game with 60fps, a resolution of 320x240, scaled
// up by a factor of 2
//ig.main( '#canvas', MyGame, 60, 1280, 720, 1 );
//ig.main( '#canvas', MyGame, 60, 800, 600, 1 );
ig.main( '#canvas', MyGame, 60, 512, 384, 1 );

});

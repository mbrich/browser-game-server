ig.module
(
	'game.entities.player' 
)
.requires
(
	'impact.entity'
)
.defines( function() 
{

	EntityPlayer = ig.Entity.extend( 
	{
		bgEntityId : 0,
		size : { x : 204, y : 204 },				// set animation size 
		health: 10,
		collides: ig.Entity.COLLIDES.PASSIVE,
		type : ig.Entity.TYPE.B,
		Speed : 100,
		maxVel: {x: 1000, y: 1000},
		updatePosTimer : new ig.Timer( 0.1 ),

		

		init : function( x, y, settings )
		{
			this.bgEntityId = settings.EntityID;

			console.log('bgEntityId ' + this.bgEntityId);

			if ( !ig.global.wm )
			{

				// Sprites for the Puppet's Animations
				this.animWalk = new ig.AnimationSheet( 'media/player/walk.png', 204, 204 );
				this.animIdle = new ig.AnimationSheet( 'media/player/idle.png', 204, 204 );
				
				this.anims.idle = new ig.Animation( this.animIdle, 0.2,
													[0, 1, 2, 3, 4, 5, 6, 7] );
				this.anims.walk = new ig.Animation( this.animWalk, 0.1, 
										[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);
				
				this.currentAnim = this.anims.walk;

			}

			this.zIndex = 99;
			this.parent( x, y, settings );

		},

		update : function()
		{
			this.parent();

			//function needs to be here because
			//if player is dead, we don't do any other update functionality
			if (this.health <= 0) 
			{
				this.kill();
				return;
			}
				
			if ( ig.input.pressed('up') )
			{
				this.vel.y = -this.Speed;
				this.currentAnim = this.anims.walk;
				DefaultCmds.sndPlayerKeydown(this.bgEntityId, KEY.UP_ARROW);

			}
			else if (ig.input.pressed('down') )
			{
				this.vel.y = this.Speed; 
				this.currentAnim = this.anims.walk;
				DefaultCmds.sndPlayerKeydown(this.bgEntityId, KEY.DOWN_ARROW);
				
			}
			else if ( ig.input.released( 'down' ) || ig.input.released( 'up' ) )
			{
				this.vel.y = 0;
				DefaultCmds.sndPlayerKeyup(this.bgEntityId, KEY.UP_ARROW);
			}
			
			if ( ig.input.pressed('left') )
			{
				this.vel.x = -this.Speed; 
				this.currentAnim = this.anims.walk;
				this.anims.walk.flip.x = true;
				DefaultCmds.sndPlayerKeydown(this.bgEntityId, KEY.LEFT_ARROW);

			}
			else if (ig.input.pressed('right') )
			{
				this.vel.x = this.Speed; 
				this.currentAnim = this.anims.walk;
				this.anims.walk.flip.x = false;
				DefaultCmds.sndPlayerKeydown(this.bgEntityId, KEY.RIGHT_ARROW);

			}
			else if ( ig.input.released( 'left' ) || ig.input.released( 'right' ) )
			{
				this.vel.x = 0;
				DefaultCmds.sndPlayerKeyup(this.bgEntityId, KEY.LEFT_ARROW);
			}

			if ( this.vel.x == 0 && this.vel.y == 0 )
			{
				this.currentAnim = this.anims.idle;
				this.anims.idle.flip.x = this.anims.walk.flip.x;
			}

			if (ig.input.pressed( 'a' ))
			{
				DefaultCmds.sndPlayerKeyup(this.bgEntityId, KEY.A);
			};

		},

		draw : function()
		{
			this.parent();
		},

		handleMovementTrace: function( res )
		{
			if (res.collision.slope)
			{
				res.collision.slope = false;
			}

			if(!res.collision.x)
			{
				if (ig.input.state('left'))
				{
					this.vel.x = -this.Speed; 
					this.currentAnim = this.anims.walk;
					this.anims.walk.flip.x = true;
				}
				if (ig.input.state('right'))
				{
					this.vel.x = this.Speed; 
					this.currentAnim = this.anims.walk;
					this.anims.walk.flip.x = false;
				}
			}

			if(!res.collision.y)
			{
				if (ig.input.state('up'))
				{
					this.vel.y = -this.Speed; 
					this.currentAnim = this.anims.walk;
				}
				if (ig.input.state('down'))
				{
					this.vel.y = this.Speed; 
					this.currentAnim = this.anims.walk;
				}
			}

			this.parent(res);
		}

	});
});
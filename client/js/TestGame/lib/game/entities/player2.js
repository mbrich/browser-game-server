ig.module
(
	'game.entities.player2' 
)
.requires
(
	'impact.entity'
)
.defines( function() 
{

	EntityPlayer2 = ig.Entity.extend( 
	{
		clientId : 0,
		size : { x : 204, y : 204 },				// set animation size 
		health: 10,
		collides: ig.Entity.COLLIDES.PASSIVE,
		type : ig.Entity.TYPE.B,
		Speed : 100,
		maxVel: {x: 1000, y: 1000},
		virtualKeyboard : null,
		updateTimer	: new ig.Timer( 3 ),

		

		init : function( x, y, settings )
		{
			this.clientId = settings["EntityID"];

			if ( !ig.global.wm )
			{

				// Sprites for the Puppet's Animations
				this.animWalk = new ig.AnimationSheet( 'media/player/walk.png', 204, 204 );
				this.animIdle = new ig.AnimationSheet( 'media/player/idle.png', 204, 204 );
				
				this.anims.idle = new ig.Animation( this.animIdle, .2, [0, 1, 2, 3, 4, 5, 6, 7] );
				this.anims.walk = new ig.Animation( this.animWalk, .1, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);
				
				this.currentAnim = this.anims.idle;

        	}

        	this.zIndex = 99;
			this.parent( x, y, settings );

			this.virtualKeyboard = new bgVirtualKeyboard( this );

			this.virtualKeyboard.keyDown = function(keyCode)
			{
				if (KEY.LEFT_ARROW == keyCode)
				{
					this.entity.currentAnim = this.entity.anims.walk;
					this.entity.vel.x = -this.entity.Speed;
					this.entity.anims.walk.flip.x = true;
				}
				if (KEY.UP_ARROW == keyCode )
				{
					this.entity.currentAnim = this.entity.anims.walk;
					this.entity.vel.y = -this.entity.Speed;
				}
				if (KEY.RIGHT_ARROW == keyCode )
				{
					this.entity.currentAnim = this.entity.anims.walk;
					this.entity.vel.x = this.entity.Speed;
					this.entity.anims.walk.flip.x = false;
				}
				if (KEY.DOWN_ARROW == keyCode )
				{
					this.entity.currentAnim = this.entity.anims.walk;
					this.entity.vel.y = this.entity.Speed;
				}
			}

			this.virtualKeyboard.keyUp = function(keyCode)
			{
				if (KEY.LEFT_ARROW == keyCode || 
					KEY.RIGHT_ARROW == keyCode )
				{
					this.entity.vel.x = 0;
				}

				if (KEY.DOWN_ARROW == keyCode || 
					KEY.UP_ARROW == keyCode )
				{
					this.entity.vel.y = 0;
				};
			}

		},

		update : function()
		{
			this.parent();

			//function needs to be here because
			//if player is dead, we don't do any other update functionality
			if (this.health <= 0) 
			{
				this.kill();
				return;
			}

			if (this.updateTimer.delta() >= 0) 
			{
				this.updateTimer.reset();
				
			}
			
			if ( this.vel.x == 0 && this.vel.y == 0 )
			{
				this.currentAnim = this.anims.idle;
				this.anims.idle.flip.x = this.anims.walk.flip.x;
			}

		},

		draw : function()
		{
			this.parent()
		},

	});
});
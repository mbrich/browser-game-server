ig.module
(
  'plugins.bgPlugin'
)
.requires
(
  'impact.impact'
)
.defines(function() 
{
	ig.Bgplugin = ig.Class.extend
	({
		updatePosTimer	: new ig.Timer( 10 ),
		retryCount		: 0,
		addPlayer		: false,

		init : function ()
		{
			// Make connection to server and test connection
			if ( !bgClient.connect() )
			{
				alert("Connection failed: WebSocket not " +
							"supported by browser");
				//try and reconnect 10 times then kill
				// make var in main that kills game

			}

			//make other init checks and things
			//add player to world
			this.addPlayer = true;
		},

		update : function ()
		{
			// Always check if the connection is open
			if (bgSocket.WebSocket.readyState == socketState.OPEN)
			{
				if (this.updatePosTimer.delta() >= 0) 
				{
					//DefaultCmds.sndPlayerPos(1, 2);
					//DefaultCmds.sndCloseClient();
					this.updatePosTimer.reset();
					
				}

				if (this.addPlayer)
				{
					DefaultCmds.sndJoinWorld();
					this.addPlayer = false;
				};
			}
			else
			{
				//kill game gracefully or try and reconnect?
				//console.log("Socket is not open");
			}
		}

		// Add other functions here

	});
  
});



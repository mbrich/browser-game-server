// <------------ Default Cmds -------------------------------------->
// 0 - 99
// Basic socket actions
bgClient.registerCmd(DefaultCmdCodes.messageToClent,
					DefaultCmds.messageToClent );

bgClient.registerCmd(DefaultCmdCodes.rcvCloseClient,
					DefaultCmds.rcvCloseClient );

bgClient.registerCmd(DefaultCmdCodes.sndCloseClient,
					DefaultCmds.sndCloseClient );

bgClient.registerCmd(DefaultCmdCodes.sndPong,
					DefaultCmds.sndPong );

bgClient.registerCmd(DefaultCmdCodes.rcvPing,
					DefaultCmds.rcvPing );


// 100 - 999
// JavaScript Event Support for entities
bgClient.registerCmd(DefaultCmdCodes.rcvEntityKeypress,
					DefaultCmds.rcvEntityKeypress );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityKeyup,
					DefaultCmds.rcvEntityKeyup );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityKeydown,
					DefaultCmds.rcvEntityKeydown );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityDragstart,
					DefaultCmds.rcvEntityDragstart );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityDragover,
					DefaultCmds.rcvEntityDragover );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityFocus,
					DefaultCmds.rcvEntityFocus );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityBlur,
					DefaultCmds.rcvEntityBlur );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityClick,
					DefaultCmds.rcvEntityClick );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityDblClick,
					DefaultCmds.rcvEntityDblClick );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityMouseover,
					DefaultCmds.rcvEntityMouseover );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityMouseout,
					DefaultCmds.rcvEntityMouseout );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityMousewheel,
					DefaultCmds.rcvEntityMousewheel );


bgClient.registerCmd(DefaultCmdCodes.sndPlayerKeypress,
					DefaultCmds.sndPlayerKeypress );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerKeyup	,
					DefaultCmds.sndPlayerKeyup );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerKeydown,
					DefaultCmds.sndPlayerKeydown );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerDragstart,
					DefaultCmds.sndPlayerDragstart );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerDragover,
					DefaultCmds.sndPlayerDragover );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerFocus,
					DefaultCmds.sndPlayerFocus );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerBlur,
					DefaultCmds.sndPlayerBlur );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerClick,
					DefaultCmds.sndPlayerClick );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerDblClick,
					DefaultCmds.sndPlayerDblClick );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerMouseover,
					DefaultCmds.sndPlayerMouseover );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerMouseout,
					DefaultCmds.sndPlayerMouseout );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerMousewheel,
					DefaultCmds.sndPlayerMousewheel );


// General Entity Updates
bgClient.registerCmd(DefaultCmdCodes.rcvEntityPos,
					DefaultCmds.rcvEntityPos );

bgClient.registerCmd(DefaultCmdCodes.rcvRemoveEntity,
					DefaultCmds.rcvRemoveEntity );

bgClient.registerCmd(DefaultCmdCodes.rcvCreateEntity,
					DefaultCmds.rcvCreateEntity );

bgClient.registerCmd(DefaultCmdCodes.rcvEntityHP,
					DefaultCmds.rcvEntityHP );

bgClient.registerCmd(DefaultCmdCodes.sndEntityStats,
					DefaultCmds.sndEntityStats );


// 500 - 599
// Player Functions
bgClient.registerCmd(DefaultCmdCodes.sndJoinWorld,
					DefaultCmds.sndJoinWorld );

bgClient.registerCmd(DefaultCmdCodes.rcvLoadWorld,
					DefaultCmds.rcvLoadWorld );

bgClient.registerCmd(DefaultCmdCodes.sndleaveWorld,
					DefaultCmds.sndleaveWorld );

bgClient.registerCmd(DefaultCmdCodes.sndJoinLevel,
					DefaultCmds.sndJoinLevel );

bgClient.registerCmd(DefaultCmdCodes.rcvLoadLevel,
					DefaultCmds.rcvLoadLevel );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerHP,
					DefaultCmds.sndPlayerHP );

bgClient.registerCmd(DefaultCmdCodes.rcvPlayerHP,
					DefaultCmds.rcvPlayerHP );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerPos,
					DefaultCmds.sndPlayerPos );

bgClient.registerCmd(DefaultCmdCodes.rcvPlayerPos,
					DefaultCmds.rcvPlayerPos );

bgClient.registerCmd(DefaultCmdCodes.sndPlayerStats,
					DefaultCmds.sndPlayerStats );

bgClient.registerCmd(DefaultCmdCodes.rcvInitPlayer,
					DefaultCmds.rcvInitPlayer );

bgClient.registerCmd(DefaultCmdCodes.sndchangeLevel,
					DefaultCmds.sndchangeLevel );





